import {
  testProps,
  testPropsDefaults,
  testSCSS,
} from "../support/utils/react/tests";
import { mount } from "../support/utils/nextjs/utils";
import { findDeep } from "../support/utils";
import CustomError from "../support/errors/CustomError";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "How",
  path: "src/components/how/How.jsx",
  props: {
    title: "How it works",
    description: `Discover, collect, and sell extraordinary NFTs
      on the world's first & largest NFT marketplace. There are  three things you'll need in place to open your account and start buying or selling NFTs on BUM.`,
    items: [
      {
        title: "Digital Currency",
        description:
          "You can get ETH, the digital currency that fuels transactions on the Ethereum blockchain, from a digital currency exchange",
      },
      {
        title: "Crypto Wallet",
        description:
          "A crypto wallet, such as MetaMask, stores your ETH and processes transactions on the Ethereum blockchain.",
      },
      {
        title: "BUM.",
        description:
          "Let's connect your wallet to BUM, edit your profile, and begin interacting in the space.",
      },
    ],
    link: "https://app.boom.dev/",
  },
};

const css = {
  path: "src/components/how/How.module.scss",
};

context("task-113", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["title", "description", "items", "link"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "items",
        default: [],
      },
    ],
  });

  testSCSS({
    ...css,
  });

  it(`should have used Grid from MUI`, () => {
    mount({ ...component });

    cy.react("How").then((comp) => {
      cy.wrap(comp)
        .get("[class*=MuiGrid]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: `Не е използван Grid от Material UI`,
              tips: [
                `Увери се, че използваш Grid според условието.`,
                `Увери се, че използваш Grid container и Grid item.`,
              ],
            })
          ).to.exist;
        });
    });
  });

  it(`should have used Grid items from MUI`, () => {
    mount({ ...component });

    cy.react("How").then((comp) => {
      cy.wrap(comp)
        .get("[class*=MuiGrid-item]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: `Не са използвани Grid items от Material UI`,
              tips: [`Увери се, че използваш Grid items според условието.`],
            })
          ).to.exist;
        });
    });
  });

  it("should use the HowStep component and it should be rendered as many times as there are 'items' coming from props", () => {
    cy.react("HowStep").then((comp) => {
      cy.wrap(comp).then((el) => {
        expect(
          el?.length,
          new CustomError({
            issue:
              "HowStep компонентът не е използван както трябва. Трябва да се мапне толкова пъти колкото е дължината на 'items' масива от props",
            tips: [
              "Увери се, че мапваш HowStep компонентът спрямо всеки item от items масива, който идва от props",
            ],
          })
        ).to.eql(component.props.items.length);
      });
    });
  });

  it(`should use scss variable $primary_main to set the background for the numbers in HowStep component`, () => {
    cy.task("readFileSync", {
      file: "src/components/how/HowStep.module.scss",
    }).then((content) => {
      expect(
        content,
        new CustomError({
          issue:
            "Не е използван colors variable-a $primary_main за сетване на background-color на номератa в HowStep",
          tips: [
            "Увери се, че си ипмопртнал colors от styles директорията и използваш rgba функция за сетване на цвета на номерата",
            "Увери се, че ползваш 10% opacity в rgba функцията за сетване на background-color",
          ],
        })
      ).to.include("background-color: rgba(colors.$primary_main, 10%)");
    });
  });

  it(`should have a functioning button with the correct href`, () => {
    cy.task("readJSFileSync", { file: component.path, jsx: true }).then(
      (data) => {
        const result = findDeep(data, {
          name: {
            name: "href",
            type: "JSXIdentifier",
          },
        }).deeper({
          expression: {
            name: "link",
            type: "Identifier",
          },
        });

        expect(
          result,
          new CustomError({
            issue: 'Не е подаден като href "link" prop-a в Learn more бутона',
            tips: [
              'Увери се, че използваш href атрибута на бутона и подаваш "link" prop',
            ],
          })
        ).to.exist;
      }
    );
  });
});
