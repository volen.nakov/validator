const esprima = require("esprima");
const CustomError = require("../support/errors/CustomError");
const findRequires = require("find-requires");

context("task-87", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have ramda installed in package.json", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.ramda,
        new CustomError({
          issue: "Ramda пакетът не е инсталиран",
          tips: [
            "Увери се, че си инсталирал ramda package",
            "Може да използваш командата npm install --save <package_name>",
          ],
        })
      ).to.exist;
    });
  });

  it("should import ramda", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      const tokens = esprima.parseModule(data);
      const imports = tokens.body.filter(
        ({ type }) => type === "ImportDeclaration"
      );
      const hasRamdaImport = Boolean(
        imports.find((line) => line?.source?.value === "ramda")
      );

      const requires = findRequires(data);
      const hasRamdaRequire = Boolean(
        requires.find((require) => require === "ramda")
      );

      expect(
        hasRamdaRequire || hasRamdaImport,
        new CustomError({
          issue: "Ramda пакетът не е импортнат или require-нат",
          tips: ["Увери се, че import-нал ramda с require или import"],
        })
      ).to.be.true;
    });
  });

  it("should use the ramda pluck method", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не се използва ramda pluck метода",
          tips: [
            "Увери се, че си наименовал pluck правилно",
            "Увери се, че си import-нал ramda и използваш правилно pluck",
          ],
        })
      ).to.include(".pluck(");
    });
  });

  it("should have 3 articles", () => {
    cy.get("article").then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Няма 3 articles елемента в HTML документа",
          tips: [
            "Увери се, че не си модифицирал HTML документа и има 3 articles като всеки от тях има първоначален клас message",
          ],
        })
      ).to.have.length(3);
    });
  });
});
