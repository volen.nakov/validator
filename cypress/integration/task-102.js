const CustomError = require("../support/errors/CustomError");
const { findDeep } = require("../support/utils");

context("task-102", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have Framer Motion in the dependencies in package.json", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.["framer-motion"],
        new CustomError({
          issue: "Библиотеката Framer Motion не е инсталирана",
          tips: [
            "Увери се, че си инсталирал Framer Motion тъй като ще ти е необходим за решението на задачата",
            "Може да използваш командата npm install --save <package_name>",
          ],
        })
      ).to.exist;
    });
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should be using motion.form to animate the form", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Не е използван motion за анимиране на формата",
          tips: [
            "Увери се, че си импортнал motion от Framer Motion и го използваш, за да анимираш формата",
          ],
        })
      ).to.include("<motion.form");
    });
  });

  it("should use initial prop to animate the enter animation", () => {
    cy.task("readJSFileSync", { file: "src/App.js", jsx: true }).then(
      (data) => {
        const found = findDeep(data, {
          type: "JSXIdentifier",
          name: "initial",
        });

        expect(
          found,
          new CustomError({
            issue:
              "Не е използван initial prop, за да се създаде анимация, когато компонента се mount-ва",
            tips: [
              "Увери се, че използваш initial prop и в отварящия таг на формата",
            ],
          })
        ).to.exist;
      }
    );
  });

  it("should use animate prop to position the element after the enter animation", () => {
    cy.task("readJSFileSync", { file: "src/App.js", jsx: true }).then(
      (data) => {
        const found = findDeep(data, {
          type: "JSXIdentifier",
          name: "animate",
        });

        expect(
          found,
          new CustomError({
            issue: "Не е използван animate prop",
            tips: [
              "Увери се, че използваш animate prop, за да позиционираш формата в средата на viewport-a",
            ],
          })
        ).to.exist;
      }
    );
  });

  it("should have x object property", () => {
    cy.task("readJSFileSync", { file: "src/App.js", jsx: true }).then(
      (data) => {
        const x = findDeep(data, {
          type: "ObjectExpression",
        }).deeper({
          name: "x",
          type: "Identifier",
        });

        expect(
          x,
          new CustomError({
            issue: "Не е използван x prop",
            tips: [
              "Увери се, че x property-то в initial и animate props на motion компонента",
            ],
          })
        ).to.exist;
      }
    );
  });

  it("should change the translateX property", () => {
    cy.get("form")
      .invoke("attr", "style")
      .then((style) => {
        expect(style).to.not.contain("translateX(0");
      });

    cy.wait(3000);

    cy.get("form")
      .invoke("attr", "style")
      .then((style) => {
        expect(style).to.contain("none");
      });
  });
});
