const CustomError = require("../support/errors/CustomError");

context("task-49", () => {
  it("Should have an input with a name 'ccname' and a type 'text'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="ccname"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'text'",
            "Увери се, че input-a има name атрибут, което е 'ccname'",
          ],
        })
      ).to.have.attr("type", "text");
    });
  });

  it("Should have an input with a name 'cardnumber' and a type 'number'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="cardnumber"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут cardnumber",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'number'",
            "Увери се, че input-a има name атрибут, което е 'cardnumber'",
          ],
        })
      ).to.have.attr("type", "number");
    });
  });

  it("Should have an input with a name 'cc-exp' and a type 'number'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="cc-exp"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут cc-exp",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'number'",
            "Увери се, че input-a има name атрибут, което е 'cc-exp'",
          ],
        })
      ).to.have.attr("type", "number");
    });
  });

  it("Should have an input with a name 'cvc' and a type 'number'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="cvc"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут cvc",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'number'",
            "Увери се, че input-a има name атрибут, което е 'cvc'",
          ],
        })
      ).to.have.attr("type", "number");
    });
  });
});
