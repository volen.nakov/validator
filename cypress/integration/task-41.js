const CustomError = require("../support/errors/CustomError");

context("task-41", () => {
  it("should have the appropriate meta tag", () => {
    cy.visit(Cypress.config("url"));

    cy.get('meta[name="viewport"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва meta tag-a за viewport",
          tips: [
            "Увери се, че си добавил meta tag с име 'viewport'",
            "Увери се, че си добавил атрибут content със съдържание 'width=device-width, initial-scale=1'",
          ],
        })
      ).to.have.attr("content", "width=device-width, initial-scale=1");
    });
  });
});
