import { findDeep } from "../support/utils";

const CustomError = require("../support/errors/CustomError");
const methods = [
  "_load",
  "_create",
  "_startLoading",
  "_stopLoading",
  "_render",
];
const pages = [1, 2, 3, 4, 5, 6];

Cypress.config("requestTimeout", 30 * 1000);
Cypress.config("responseTimeout", 30 * 1000);

context("task-89", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have JS app", () => {
    cy.getJsApp(new CustomError(CustomError.common.JS_APP_NOT_FOUNT)).then(
      (app) => expect(app).to.exist
    );
  });

  methods.forEach((method) => {
    it(`should have a ${method} method in Application.js`, () => {
      cy.getJsApp().then((app) => {
        expect(
          app[method],
          new CustomError({
            issue: `Липсва методът ${method}`,
            tips: [
              `Увери се, че си изписал името на метода правилно ("${method}")`,
              `Увери се, че метода е дефиниран в класа Application.js`,
              `Увери се, че метода е ES Class method`,
            ],
          })
        ).to.be.a("function");
      });
    });
  });

  it(`should have a _loading property in Application.js which is a HTMLProgressElement`, () => {
    cy.getJsApp().then((app) => {
      expect(
        app._loading.constructor.name,
        new CustomError({
          issue: `_loading не е HTMLProgressElement или липсва изцяло`,
          tips: [
            `Увери се, че си изписал името на property-то правилно ("_loading")`,
            `Увери се, че property-то е дефинирано в класа Application.js`,
            `Увери се, че property-то е закачено на инстанцията и не е статично`,
            `Увери се, че property-то е <progress> елемент`,
          ],
        })
      ).to.eq("HTMLProgressElement");
    });
  });

  it(`Should have used fetch inside the _load method`, () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            name: "_load",
            type: "Identifier",
          },
          kind: "method",
        }).deeper({
          callee: { type: "Identifier", name: "fetch" },
        });

        expect(
          node,
          new CustomError({
            issue: `Не е използван fetch в _load метода`,
            tips: [
              `Увери се, че си използвал fetch https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API`,
              `Увери се, че си използвал fetch в точно _load метода`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should have used await inside the _load method`, () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            name: "_load",
            type: "Identifier",
          },
          kind: "method",
        }).deeper({
          type: "AwaitExpression",
        });

        expect(
          node,
          new CustomError({
            issue: `Не е използван await в _load метода`,
            tips: [
              `Увери се, че си използвал await https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function`,
              `Увери се, че си използвал await в точно _load метода`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should a _load method in Application.js which is async`, () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            name: "_load",
            type: "Identifier",
          },
          kind: "method",
        });

        expect(
          node.value.async,
          new CustomError({
            issue: `_load не е дефиниран като асинхронен (async)`,
            tips: [
              `Увери се, че си използвал async https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function`,
              `Увери се, че си използвал async в при дефиницията на _load метода`,
            ],
          })
        ).to.be.true;
      }
    );
  });

  pages.forEach((page) => {
    it(`Should have fetched the data the API page ${page}`, () => {
      cy.visit(Cypress.config("url"));
      const url = page === 1 ? `/planets` : `/planets?page=${page}`;
      cy.intercept(url).as(`page${page}`);
      cy.wait(`@page${page}`);
    });
  });

  it(`Should have a hidden progressbar when the loading stops`, () => {
    cy.visit(Cypress.config("url"));
    cy.intercept(`/planets?page=6`).as(`page`);
    cy.wait(`@page`);
    cy.get(".progress").then((el) => {
      expect(
        el,
        new CustomError({
          issue: `Loading bar-a не е скрит`,
          tips: [
            `Увери се, си скрил правилния елемент (.progress)`,
            `Увери се, че си скрил елемента с display: none`,
          ],
        })
      ).to.be.hidden;
    });
  });
});
