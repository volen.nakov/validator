const CustomError = require("../support/errors/CustomError");
const { findDeep } = require("../support/utils");

context("task-88", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("shouldn the latest version installed", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.animejs,
        new CustomError({
          issue: "Animejs пакетът не е update-нат",
          tips: [
            "Увери се, че си update-нал animejs package до версия 3.2.1",
            "Може да използваш командата npm update",
          ],
        })
      ).to.be.eq("^3.2.1");
    });
  });

  it("should have JS app", () => {
    cy.getJsApp(new CustomError(CustomError.common.JS_APP_NOT_FOUNT)).then(
      (app) => expect(app).to.exist
    );
  });

  it("should have an addEventListener which listens for a click event", () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const hasEventListener = Boolean(
          findDeep(data, {
            type: "Identifier",
            name: "addEventListener",
          })
        );

        const hasClickLiteral = Boolean(
          findDeep(data, {
            type: "Literal",
            value: "click",
          })
        );

        expect(
          hasEventListener && hasClickLiteral,
          new CustomError({
            issue: "Не е закачен click event на addEventListener",
            tips: [
              "Увери се, че си добавил click като първи аргумент на addEventListener",
              "Увери се, че си селектирал article елемента и си му добавил addEventListener",
              "Увери се, че си използвал querySelector, за да селектираш елемента",
              "Увери се, че закачаш event listener директно на резултата от querySelector без да го assign-ваш на променлива",
            ],
          })
        ).to.be.true;
      }
    );
  });

  it("should have easing property with spring value", () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            type: "Identifier",
            name: "easing",
          },
        }).deeper({
          type: "Literal",
        });

        expect(
          node?.value,
          new CustomError({
            issue: "Не е използвана spring easing анимация",
            tips: [
              "Увери се, че си ъпдейтнал версията на anime.js и използваш easing animation със spring",
            ],
          })
        ).to.include("spring");
      }
    );
  });
});
