const CustomError = require("../support/errors/CustomError");

context("task-104", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  it("should load a nextjs app", () => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact(1000, "#__next");
  });

  it(`should have a package.json file with the name "nextjs"`, () => {
    cy.task("readJSONFileSync", {
      file: "package.json",
    }).then((data) => {
      expect(
        data.name,
        new CustomError({
          issue: `Името на проекта в package.json не е nextjs`,
          tips: ["Увери се, че си import-нал правилния проект"],
        })
      ).to.eq("nextjs");
    });
  });

  it(`should have a package.json file with a start script`, () => {
    cy.task("readJSONFileSync", {
      file: "package.json",
    }).then((data) => {
      cy.log(data);

      expect(
        data.scripts.start,
        new CustomError({
          issue: `Скрипта за пускане на проекта не е правилен`,
          tips: ["Увери се, че си import-нал правилния проект"],
        })
      ).to.eq("next -p 8080");
    });
  });
});
