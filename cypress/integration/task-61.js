const CustomError = require("../support/errors/CustomError");

context("task-61", () => {
  it("Should have a fire emoji inside of each hot price", () => {
    cy.visit(Cypress.config("url"));

    cy.get(".price.hot").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Цената не съдържа fire emoji 🔥",
          tips: [
            "Увери се, си добавил fire emoji в елемента с клас 'price'",
            "Увери се, че си добавил правилното emoji",
          ],
        })
      ).to.contain("🔥");
    });
  });

  it("Should use querySelectorAll for selecting the elements", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е използван querySelectorAll за селектиране на елементите",
          tips: [
            "Увери се, че използваш querySelectorAll вместо querySelector, за да селектираш всички елементи",
            "Увери се, че си добавил javascript кода в app.js",
          ],
        })
      ).to.include(`document.querySelectorAll`);
    });
  });
});
