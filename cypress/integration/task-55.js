const CustomError = require("../support/errors/CustomError");

context("task-55", () => {
  it("should have a div with a class name container", () => {
    cy.visit(Cypress.config("url"));
    cy.get(".container")
      .then(($div) => {
        expect(
          $div,
          new CustomError({
            issue: "Не съществува div с клас container",
            tips: ["Увери се, че имаш div html елемент, който има сетнат клас container"],
          })
        ).to.exist;
      });
  });
  it("should have a display property of flex", () => {
    cy.get(".container").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Div-ът с клас container няма display проперти flex",
          tips: [
            "Увери се, че си сетнал display проперти flex на div-a с клас container",
          ],
        })
      ).to.have.css("display", "flex");
    });
  });
  it("should have an aside with a class name sidebar", () => {
    cy.get("aside").then(($aside) => {
      expect(
        $aside,
        new CustomError({
          issue: "Aside бар-ът няма class sidebar",
          tips: [
            "Увери се, че използваш aside html element",
            "Увери се, че 'aside' елементът е изписан правилно",
          ],
        })
      ).to.have.class("sidebar");
    });
  });
  it("should have an aside with a width of 280px", () => {
    cy.get("aside.sidebar").then(($aside) => {
      expect(
        $aside,
        new CustomError({
          issue: "Aside бар-ът няма width 280px",
          tips: [
            "Увери се, че aside елементът има широчина от 280px",
            "Може да сетнеш широчината на елемента с 'width' property-то ",
          ],
        })
      ).to.have.css("width", "280px");
    });
  });
  it("should have a main with a class name main", () => {
    cy.get("main").then(($main) => {
      expect(
        $main,
        new CustomError({
          issue: "Main елементът трябва да има клас main",
          tips: [
            "Увери се, че си добавил 'main' html eлемент",
            "Увери се, че main елементът има клас main",
          ],
        })
      ).to.have.class("main");
    });
  });
});
