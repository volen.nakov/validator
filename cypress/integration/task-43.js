const CustomError = require("../support/errors/CustomError");

context("task-43", () => {
  it("should have a title with the text 'No new messages'", () => {
    cy.visit(Cypress.config("url"));

    cy.title().then((title) => {
      expect(
        title,
        new CustomError({
          issue: "Не е правилен title tag-a",
          tips: [
            "Увери се, че title tag-a съществува",
            "Увери се, текста на title tag-a е 'No new messages'",
          ],
        })
      ).to.eq("No new messages");
    });
  });

  it("should have a title with the text 'One new message'", () => {
    cy.wait(5000);

    cy.title().then((title) => {
      expect(
        title,
        new CustomError({
          issue: "Не е правилен title tag-a",
          tips: [
            "Увери се, че title tag-a съществува",
            "Увери се, текста на title tag-a е 'One new message'",
          ],
        })
      ).to.eq("One new message");
    });
  });
});
