const CustomError = require("../support/errors/CustomError");

context("task-56", () => {
  it("should have a div with a class name container", () => {
    cy.visit(Cypress.config("url"));
    cy.get(".container").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Не съществува div с клас container",
          tips: [
            "Увери се, че използваш div html element",
            "Увери се, че имаш div, който има клас container",
          ],
        })
      ).to.exist;
    });
  });
  it("should have a display property of flex", () => {
    cy.get(".container").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Div-ът с клас container няма display проперти flex",
          tips: [
            "Увери се, че си сетнал display проперти flex на div-a с клас container",
          ],
        })
      ).to.have.css("display", "flex");
    });
  });
  it("should have a nav element with a class toolbar", () => {
    cy.get("nav").then(($nav) => {
      expect(
        $nav,
        new CustomError({
          issue: "Няма nav елемент с клас toolbar",
          tips: [
            "Увери се, че си добавил 'nav' html елемент",
            "Увери се, че си добавил клас toolbar на nav елемента",
          ],
        })
      ).to.have.class("toolbar");
    });
  });
  it("should have a height of 80px", () => {
    cy.get("nav.toolbar")
      .then(($nav) => {
        expect(
          $nav,
          new CustomError({
            issue: "Nav елементът с клас toolbar няма височина от 80px",
            tips: ["Увери се, че си добавил height проперти от 80px"],
          })
        ).to.have.css("height", "80px");
      })

  });
  it("should have a main with a class name main", () => {
    cy.get("main").then(($main) => {
      expect(
        $main,
        new CustomError({
          issue: "Main елементът трябва да има клас main",
          tips: [
            "Увери се, че си добавил main eлемент",
            "Увери се, че main елементът има клас main",
          ],
        })
      ).to.have.class("main");
    });
  });
});
