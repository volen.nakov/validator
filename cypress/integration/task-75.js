const CustomError = require("../support/errors/CustomError");

const textSample = "this is a text that should be saved";

context("task-75", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should use localStorage", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Не се използва localStorage",
          tips: [
            "Увери се, че запазваш съдържанието на бележката в localStorage",
          ],
        })
      ).to.contain("localStorage");
    });
  });

  it("should use be able to enter text in the textarea", () => {
    cy.react("App")
      .find("textarea")
      .type(textSample)
      .invoke("text")
      .then((text) => {
        expect(
          text,
          new CustomError({
            issue: "Не може да се пише в textarea-та",
            tips: [
              "Увери се, че слагаш value атрибут на textarea, равен на стейт променливата, която пази текущата стойност",
            ],
          })
        ).to.equal(textSample);
      });
  });

  it("should be able to save the text to localStorage", () => {
    cy.react("App")
      .find("button.is-primary")
      .click()
      .then(() => {
        cy.reload().then(() => {
          cy.waitForReact().then(() => {
            cy.react("App")
              .find("textarea")
              .invoke("text")
              .then((text) => {
                expect(
                  text,
                  new CustomError({
                    issue: "Текстът не е същият, който запазихме",
                    tips: [
                      "Провери value атрибута на textarea елемента",
                      "Увери се, че използваш localStorage функциите правилно",
                    ],
                  })
                ).to.equal(textSample);
              });
          });
        });
      });
  });

  it("should be able to reset the saved note", () => {
    cy.react("App")
      .find("button:not(.is-primary)")
      .click()
      .then(() => {
        cy.reload().then(() => {
          cy.waitForReact().then(() => {
            cy.react("App")
              .find("textarea")
              .invoke("text")
              .then((text) => {
                expect(
                  text,
                  new CustomError({
                    issue: "Запазеният текст не се reset-ва",
                    tips: [
                      "Провери дали Reset бутона прави нещо при клик",
                      "Увери се, че използваш localStorage функциите правилно",
                    ],
                  })
                ).to.equal("");
              });
          });
        });
      });
  });
});
