import { testMount } from "../support/utils/nextjs/tests";
import {
  testProps,
  testPropsDefaults,
  testSCSS,
} from "../support/utils/react/tests";
import { mount } from "../support/utils/nextjs/utils";
import { findDeep } from "../support/utils";
import CustomError from "../support/errors/CustomError";

const component = {
  name: "Card",
  path: "src/components/card/Card.jsx",
  props: {
    name: "Clock",
    user: { avatarUrl: "images/avatar.png", verified: true },
    mediaUrl: "images/nft.jpg",
    price: 200,
    currency: "BTC",
    likes: 20,
  },
};

const css = {
  path: "src/components/card/Card.module.scss",
};

context("task-114", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["name", "likes", "mediaUrl", "price", "currency", "timeLeft"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "likes",
        default: 0,
      },
    ],
  });

  testSCSS({
    ...css,
  });

  it("should have react-countdown installed in package.json", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.["react-countdown"],
        new CustomError({
          issue: "react-countdown пакетът не е инсталиран",
          tips: [
            "Увери се, че си инсталирал react-countdown",
            "Може да използваш командата npm install --save <package_name>",
          ],
        })
      ).to.exist;
    });
  });

  it("should have a border radius for the Live wrapper element", () => {
    mount({ ...component });

    cy.react("Card").then((comp) => {
      cy.wrap(comp)
        .find("img[class*=badge]")
        .then((el) => {
          expect(
            el?.[0],
            new CustomError({
              issue: "Live елементът няма извити ъгли",
              tips: [
                "Увери се, че използваш CSS пропърти за Live елемента, което добавя радиус на ъглите на изображението",
              ],
            })
          ).to.have.css("border-radius");
        });
    });
  });

  it(`should have used Countdown from react-countdown`, () => {
    cy.task("readJSFileSync", { file: component.path, jsx: true }).then(
      (data) => {
        const result = findDeep(data, {
          name: {
            type: "JSXIdentifier",
            name: "Countdown",
          },
        });

        expect(
          result,
          new CustomError({
            issue: `Не е използван Countdown component от react-countdown, за отброяване на оставащото време според проп-а timeLeft`,
            tips: [`Увери се, че използваш Countdown от react-countdown`],
          })
        ).to.exist;
      }
    );
  });
});
