const CustomError = require("../support/errors/CustomError");

context("task-64", () => {
  it(`Should have a 'src/faq.html' file`, () => {
    cy.task("gitListFiles").then((data) => {
      expect(
        data,
        new CustomError({
          issue: 'Липсва валидна faq.html в src"',
          tips: [
            "Увери се, че си създал faq.html на правилното място",
            "Увери се, че faq.html е валидна страница като я отвориш в browser-a",
          ],
        })
      ).to.include("src/faq.html");
    });
  });

  it(`Should have a 'src/scss/pages/faq.scss' file`, () => {
    cy.task("gitListFiles").then((data) => {
      expect(
        data,
        new CustomError({
          issue: 'Липсва файла scss/pages/faq.scss"',
          tips: ["Увери се, че си създал faq.scss на правилното място"],
        })
      ).to.include("src/scss/pages/faq.scss");
    });
  });

  it(`Should have a h1 tag in the faq page`, () => {
    cy.visit(`${Cypress.config("url")}faq.html`);

    cy.get("h1").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва h1 с текст FAQ в страницата",
          tips: [
            "Увери се, че си добавил h1 tag",
            "Увери се, че h1 tag-a съдържа 'FAQ'",
          ],
        })
      ).to.contain("FAQ");
    });
  });
});
