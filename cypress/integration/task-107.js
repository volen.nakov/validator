const CustomError = require("../support/errors/CustomError");
const { findDeep } = require("../support/utils");

context("task-107", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  it("should have an Avatar component which can be mounted", () => {
    cy.task("nextMount", {
      name: "Avatar",
      path: "src/components/avatar/Avatar.jsx",
    });
    cy.visit(Cypress.config("url"));
    cy.waitForReact(1000, "#__next");
  });

  it("should have an Avatar component that accepts a 'url' prop", () => {
    cy.task("readJSFileSync", {
      file: "src/components/avatar/Avatar.jsx",
      jsx: true,
    }).then((data) => {
      const params = findDeep(data, {
        type: "FunctionDeclaration",
      })?.params;

      const found = findDeep(params, {
        key: {
          name: "url",
          type: "Identifier",
        },
      });

      expect(
        found,
        new CustomError({
          issue: "Няма дефиниран url prop на Avatar компонента",
          tips: [
            "Увери се, че Avatar е function component",
            "Увери се, че компонентът приема prop с име url",
            "Увери се, че компонентът приема prop във формата { url }",
          ],
        })
      ).to.exist;
    });
  });

  it("should have an Avatar component that accepts a 'size' prop", () => {
    cy.task("readJSFileSync", {
      file: "src/components/avatar/Avatar.jsx",
      jsx: true,
    }).then((data) => {
      const params = findDeep(data, {
        type: "FunctionDeclaration",
      })?.params;

      const found = findDeep(params, {
        key: {
          name: "size",
          type: "Identifier",
        },
      });

      expect(
        found,
        new CustomError({
          issue: "Няма дефиниран size prop на Avatar компонента",
          tips: [
            "Увери се, че Avatar е function component",
            "Увери се, че компонентът приема prop с име size",
            "Увери се, че компонентът приема prop във формата { size }",
          ],
        })
      ).to.exist;
    });
  });

  it("should have an Avatar component that accepts a 'verified' prop", () => {
    cy.task("readJSFileSync", {
      file: "src/components/avatar/Avatar.jsx",
      jsx: true,
    }).then((data) => {
      const params = findDeep(data, {
        type: "FunctionDeclaration",
      })?.params;

      const found = findDeep(params, {
        key: {
          name: "verified",
          type: "Identifier",
        },
      });

      expect(
        found,
        new CustomError({
          issue: "Няма дефиниран verified prop на Avatar компонента",
          tips: [
            "Увери се, че Avatar е function component",
            "Увери се, че компонентът приема prop с име verified",
            "Увери се, че компонентът приема prop във формата { verified }",
          ],
        })
      ).to.exist;
    });
  });

  it("should have a default value for the 'size' prop", () => {
    cy.task("readJSFileSync", {
      file: "src/components/avatar/Avatar.jsx",
      jsx: true,
    }).then((data) => {
      const properties = findDeep(data, {
        type: "FunctionDeclaration",
      })?.params?.find(({ properties }) => Array.isArray(properties));

      const found = findDeep(properties, {
        type: "Literal",
        value: 90,
      });

      expect(
        found,
        new CustomError({
          issue: "Няма default стойност за size prop-а на Avatar компонента",
          tips: [
            "Увери се, че задаваш default стойност 90",
            "Увери се, че default стойността се задава при деструктурирането на props",
          ],
        })
      ).to.exist;
    });
  });

  it("should have a default value for the 'verified' prop", () => {
    cy.task("readJSFileSync", {
      file: "src/components/avatar/Avatar.jsx",
      jsx: true,
    }).then((data) => {
      const properties = findDeep(data, {
        type: "FunctionDeclaration",
      })?.params?.find(({ properties }) => Array.isArray(properties));

      const found = findDeep(properties, {
        type: "Literal",
        value: false,
      });

      expect(
        found,
        new CustomError({
          issue:
            "Няма default стойност за verified prop-а на Avatar компонента",
          tips: [
            "Увери се, че задаваш default стойност false",
            "Увери се, че default стойността се задава при деструктурирането на props",
          ],
        })
      ).to.exist;
    });
  });

  it("should modify Avatar component content based on the given props", () => {
    cy.task("nextMount", {
      name: "Avatar",
      props: {
        size: 300,
        verified: false,
      },
      path: "src/components/avatar/Avatar.jsx",
    });

    cy.visit(Cypress.config("url"));
    cy.waitForReact(1000, "#__next");

    cy.react("Avatar", { options: { timeout: 100000 } }).then((comp) => {
      cy.wrap(comp)
        .get("img")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: "Визуализира се повече от едно изображение",
              tips: [
                "Увери се, че, когато verified има стойност false се показва само аватарът, без verified значката",
                "Увери се, че използваш обикновен <img> таг",
              ],
            })
          ).to.have.length(1);
        });

      cy.wrap(comp)
        .get("img[class*=image]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: "Големината на аватара не се изменя спрямо size prop-a",
              tips: ["Увери се, че използваш style prop в Avatar компонента"],
            })
          ).to.have.prop("width", 300);
        });
    });

    cy.task("nextMount", {
      name: "Avatar",
      props: {
        size: 420,
        verified: true,
      },
      path: "src/components/avatar/Avatar.jsx",
    });

    cy.wait(2000);

    cy.react("Avatar", { options: { timeout: 100000 } }).then((comp) => {
      cy.wrap(comp)
        .get("img")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: "Визуализира се едно изображение",
              tips: [
                "Увери се, че, когато verified има стойност true се показва и аватарът, и verified значката",
                "Увери се, че използваш обикновен <img> таг",
              ],
            })
          ).to.have.length(2);
        });

      cy.wrap(comp)
        .get("img[class*=image]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: "Големината на аватара не се изменя спрямо size prop-a",
              tips: ["Увери се, че използваш style prop в Avatar компонента"],
            })
          ).to.have.prop("width", 420);
        });

      cy.wrap(comp)
        .get("img[class*=badge]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: "Значката verified липсва",
              tips: [
                "Увери се, че, когато verified има стойност true се показва и аватарът, и verified значката",
                "Увери се, че значката има badge клас",
                "Увери се, че използваш обикновен <img> таг",
              ],
            })
          ).to.exist;
        });
    });
  });
});
