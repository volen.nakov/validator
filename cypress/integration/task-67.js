const CustomError = require("../support/errors/CustomError");

context("task-67", () => {
  it(`Should have a mixin shadow defined in _mixins.scss`, () => {
    cy.visit(Cypress.config("url"));

    cy.task("readFileSync", { file: "src/scss/base/_mixins.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue: "Не е дефиниран shadow mixin в _mixins.scss",
            tips: [
              "Увери се, че си дефинирал mixin-a на правилното място",
              "Увери се, че си увери се, че mixin-a приема $elevation аргумент",
              "Увери се, че си mixin-a е дефиниран с правилния синтаксис",
              "Увери се, че няма space между shadow и скобите за аргумента",
            ],
          })
        )
          .to.include(`@mixin shadow`)
          .and.to.include(`($elevation)`);
      }
    );
  });

  it(`Should have the shadow mixin used in app.scss`, () => {
    cy.visit(Cypress.config("url"));

    cy.task("readFileSync", { file: "src/scss/app.scss" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е използван shadow mixin-a в app.scss",
          tips: [
            "Увери се, че си използвал shadow mixin-a по правилния начин",
            "Увери се, че че подаваш elevation на mixin-a",
          ],
        })
      ).to.include(`@include shadow(`);
    });
  });
});
