import CustomError from "../support/errors/CustomError";
import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";
import { testProps, testSCSS } from "../support/utils/react/tests";
import { findDeep } from "../support/utils";

const component = {
  name: "TopCollectors",
  path: "src/components/collectors/TopCollectors.jsx",
  props: {
    collectors: [
      {
        name: "Peter",
        info: 12312,
        avatar: "/images/avatar.png",
        verified: true,
      },
      {
        name: "John",
        info: 1111,
        avatar: "/images/avatar.png",
        verified: true,
      },
      {
        name: "Steven",
        info: 52,
        avatar: "/images/avatar.png",
        verified: true,
      },
      {
        name: "Antonio Banderas",
        info: 3,
        avatar: "/images/avatar.png",
        verified: true,
      },
      {
        name: "Donald",
        info: 12,
        avatar: "/images/avatar.png",
        verified: true,
      },
    ],
  },
};

const css = {
  path: "src/components/collectors/TopCollectors.module.scss",
};

context("task-119", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["collectors"],
  });

  testSCSS({
    ...css,
  });

  it(`should have used the Select component`, () => {
    mount({ ...component });
    cy.react("MuiSelectSelect");
  });

  it(`should have used lodash's chunk method`, () => {
    cy.task("readJSFileSync", { file: component.path, jsx: true }).then(
      (data) => {
        const result = findDeep(data, {
          property: {
            type: "Identifier",
            name: "chunk",
          },
        });

        expect(
          result,
          new CustomError({
            issue: `Не е използван chunk метода на lodash`,
            tips: [
              `Увери се, че използваш lodash и метода chunk, за да разделиш масива.`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`should have used CollectorColumn component`, () => {
    mount({ ...component });
    cy.react("CollectorColumn");
  });
});
