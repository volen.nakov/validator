const CustomError = require("../support/errors/CustomError");

import { testProps, testPropsDefaults } from "../support/utils/react/tests";

import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "Card",
  path: "src/components/card/Card.jsx",
  props: {
    name: "Clock",
    likes: 3500,
    mediaUrl: "/images/nft.jpg",
    user: {
      avatarUrl: "/images/avatar.png",
      verified: false,
    },
    price: "11.9",
    currency: "ETH",
  },
};

const css = {
  path: "src/components/card/Card.module.scss",
};

context("task-108", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["name", "likes", "mediaUrl", "price", "currency"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "likes",
        default: 0,
      },
    ],
  });

  it("should use the Avatar component", () => {
    cy.react("Avatar").then((comp) => {
      cy.wrap(comp)
        .find("img")
        .then((el) => {
          expect(
            el?.length,
            new CustomError({
              issue: "Значката verified се показва, когато не трябва",
              tips: [
                "Увери се, че user обекта, подаден като prop на Card компонента отговаря на props, които се подават на Avatar компонента",
              ],
            })
          ).to.equal(1);
        });
    });
  });

  it("should have millify installed in package.json", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.millify,
        new CustomError({
          issue: "millify пакетът не е инсталиран",
          tips: [
            "Увери се, че си инсталирал millify",
            "Може да използваш командата npm install --save <package_name>",
          ],
        })
      ).to.exist;
    });
  });

  it("should convert the likes count to k, M or B string", () => {
    mount({ ...component });

    cy.react(component.name).then((comp) => {
      cy.wrap(comp)
        .get("[class*=likes]")
        .then((el) => {
          expect(
            el?.[0]?.innerText.toLowerCase(),
            new CustomError({
              issue:
                "Лайковете не се визуализират във формат 1.1K, 2.5M и т.н.",
              tips: ["Увери се, че, използваш NPM пакета millify"],
            })
          ).to.equal("3.5k");
        });
    });
  });

  it("should display the price the same way as in image preview", () => {
    mount({ ...component });

    cy.react(component.name).then((comp) => {
      cy.wrap(comp)
        .get("[class*=price]")
        .then((el) => {
          expect(
            el?.[0]?.innerText,
            new CustomError({
              issue: "Визуализираната цена не се ъпдейтва спрямо price prop-a",
              tips: [
                "Увери се, че добавяш въничка пред цената на NFT-то",
                "Увери се, че стойността на currency prop-a се визуализира до цената на NFT-то",
              ],
            })
          ).to.equal("~11.9 ETH");
        });
    });
  });

  it("should update the Avatar component according to the user prop", () => {
    mount({
      ...component,
      props: {
        ...component.props,
        user: {
          ...component.props.user,
          verified: true,
        },
      },
    });

    cy.react("Avatar").then((comp) => {
      cy.wrap(comp)
        .get("[class*=badge]")
        .then((el) => {
          expect(
            el?.length,
            new CustomError({
              issue: "Значката verified не се показва",
              tips: [
                "Увери се, че значката verified се показва, когато user.verified има стойност true",
              ],
            })
          ).to.equal(1);
        });
    });
  });

  it("should have a border radius for the NFT image", () => {
    mount({ ...component });

    cy.react("Card").then((comp) => {
      cy.wrap(comp)
        .find("img[class*=media]")
        .then((el) => {
          expect(
            el?.[0],
            new CustomError({
              issue: "NFT-то няма извити ъгли",
              tips: [
                "Увери се, че използваш CSS пропърти за NFT-то, което добавя радиус на ъглите на изображението",
              ],
            })
          ).to.have.css("border-radius");
        });
    });
  });
});
