const CustomError = require("../support/errors/CustomError");

context("task-48", () => {
  it("Should have an input with a name 'email' and a type 'email'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="email"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут email",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'email'",
            "Увери се, че input-a има name атрибут, което е 'email'",
          ],
        })
      ).to.have.attr("type", "email");
    });
  });

  it("Should have an input with a name 'name' and a type 'name'", () => {
    cy.get('.input[name="name"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'text'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("type", "text");
    });
  });

  it("Should have an input with a name 'address' and a type 'address'", () => {
    cy.get('.input[name="address"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут address",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'text'",
            "Увери се, че input-a има name атрибут, което е 'address'",
          ],
        })
      ).to.have.attr("type", "text");
    });
  });

  it("Should have an input with a name 'phone' and a type 'phone'", () => {
    cy.get('.input[name="phone"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут phone",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'tel'",
            "Увери се, че input-a има name атрибут, което е 'phone'",
          ],
        })
      ).to.have.attr("type", "tel");
    });
  });

  it("Should have an input with a name 'color' and a type 'color'", () => {
    cy.get('.input[name="color"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут color",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'color'",
            "Увери се, че input-a има name атрибут, което е 'color'",
          ],
        })
      ).to.have.attr("type", "color");
    });
  });

  it("Should have an input with a name 'delivery_date' and a type 'date'", () => {
    cy.get('.input[name="delivery_date"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут delivery_date",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'date'",
            "Увери се, че input-a има name атрибут, което е 'delivery_date'",
          ],
        })
      ).to.have.attr("type", "date");
    });
  });
});
