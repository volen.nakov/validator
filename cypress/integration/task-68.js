const CustomError = require("../support/errors/CustomError");

context("task-68", () => {
  it(`Should have a mixin frost defined in _mixins.scss`, () => {
    cy.visit(Cypress.config("url"));

    cy.task("readFileSync", { file: "src/scss/base/_mixins.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue: "Не е дефиниран frost mixin в _mixins.scss",
            tips: [
              "Увери се, че си дефинирал mixin-a на правилното място",
              "Увери се, че си увери се, че mixin-a приема $intensity аргумент",
              "Увери се, че си mixin-a е дефиниран с правилния синтаксис",
            ],
          })
        ).to.include(`@mixin frost($intensity)`);
      }
    );
  });

  it(`Should have a mixin frost which uses backdrop-filter`, () => {
    cy.visit(Cypress.config("url"));

    cy.task("readFileSync", { file: "src/scss/base/_mixins.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue:
              "Не е използван backdrop-filter за постигането на frost ефекта",
            tips: [
              "Увери се, че си дефинирал mixin-a на правилното място",
              "Увери се, че използваш backdrop-filter css property-то",
            ],
          })
        ).to.include(`backdrop-filter: blur(`);
      }
    );
  });

  it(`Should have the frost mixin used in app.scss`, () => {
    cy.visit(Cypress.config("url"));

    cy.task("readFileSync", { file: "src/scss/app.scss" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е използван frost mixin-a в app.scss",
          tips: [
            "Увери се, че си използвал frost mixin-a по правилния начин",
            "Увери се, че че подаваш intensity на mixin-a",
          ],
        })
      ).to.include(`@include frost(`);
    });
  });
});
