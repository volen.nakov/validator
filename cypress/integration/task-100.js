const CustomError = require("../support/errors/CustomError");

context("task-100", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should have inline css on all 4 elements where its needed", () => {
    cy.task("readFileSync", {
      file: "src/App.js",
    }).then((data) => {
      const styles = data.split("style={{");
      expect(
        styles,
        new CustomError({
          issue: "Не всеки елемент има inline css",
          tips: [
            "Увери се, че добавяш inline css на wrapper, mealHeading, description и price елементите",
          ],
        })
      ).to.have.length(5);
    });
  });

  it("should not have .wrapper class", () => {
    cy.get(".App > div").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Не е премахнат class-a на div елемента",
          tips: ["Увери се, че си премахнал className от div елемента"],
        })
      ).to.not.have.attr("class");
    });
  });

  it("should not have .mealHeading class", () => {
    cy.get(".App > div > h3").then(($h3) => {
      expect(
        $h3,
        new CustomError({
          issue: "Не е премахнат class-a на h3 елемента",
          tips: ["Увери се, че си премахнал className от h3 елемента"],
        })
      ).to.not.have.attr("class");
    });
  });

  it("should not have .description class", () => {
    cy.get(".App > div > div:nth-of-type(1)").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Не e премахнат class-a на div елементите за description",
          tips: ["Увери се, че си премахнал className от всички елементи"],
        })
      ).to.not.have.attr("class");
    });
  });

  it("should not have .price class", () => {
    cy.get(".App > div > div:nth-of-type(2)").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Не e премахнат class-a на div елементиa за price",
          tips: [
            "Увери се, че си премахнал className от div елемент-а за цена",
          ],
        })
      ).to.not.have.attr("class");
    });
  });
});
