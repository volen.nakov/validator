const CustomError = require("../support/errors/CustomError");

context("task-62", () => {
  it("Should have a $body-color variable defined in _vars.scss", () => {
    cy.task("readFileSync", { file: "src/scss/base/_vars.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue:
              "Не дефинирана променлива $body-color в src/scss/base/_vars.scss",
            tips: [
              "Увери се, че си дефинирал променливата в правилния файл",
              "Увери се, че си изписал променливата правилно",
            ],
          })
        ).to.include(`$body-color:`);
      }
    );
  });

  it("Should have a $text-color variable defined in _vars.scss", () => {
    cy.task("readFileSync", { file: "src/scss/base/_vars.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue:
              "Не дефинирана променлива $text-color в src/scss/base/_vars.scss",
            tips: [
              "Увери се, че си дефинирал променливата в правилния файл",
              "Увери се, че си изписал променливата правилно",
            ],
          })
        ).to.include(`$text-color:`);
      }
    );
  });

  it("Should have $text-color used in app.scss", () => {
    cy.task("readFileSync", { file: "src/scss/app.scss" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е използвана променливата $text-color в src/scss/app.scss",
          tips: [
            "Увери се, че си използвал променливата в правилния файл",
            "Увери се, че си изписал променливата правилно",
          ],
        })
      ).to.include(`$text-color`);
    });
  });

  it("Should have $body-color used in app.scss", () => {
    cy.task("readFileSync", { file: "src/scss/app.scss" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е използвана променливата $body-color в src/scss/app.scss",
          tips: [
            "Увери се, че си използвал променливата в правилния файл",
            "Увери се, че си изписал променливата правилно",
          ],
        })
      ).to.include(`$body-color`);
    });
  });
});
