const CustomError = require("../support/errors/CustomError");

context("task-45", () => {
  it("should have html element with the font family set to 'Roboto, sans-serif", () => {
    cy.visit(Cypress.config("url"));

    cy.get("html").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Не е зададен font-family на html елемента",
          tips: [
            "Увери се, че си задал font-family property-то на html елемента",
            "Увери се, че зададеното font-family на html елемента е 'Roboto, sans-serif'",
          ],
        })
      ).to.have.css("font-family", "Roboto, sans-serif");
    });
  });

  it("should have a roboto font file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      const match = data.some(
        (file) => file.includes("roboto") || file.includes("Roboto")
      );

      expect(
        match,
        new CustomError({
          issue: "Липсва roboto font файл в branch master",
          tips: [
            "Увери се, че roboto font файла съдържа в името си 'roboto' или 'Roboto'",
            "Увери се, че roboto font файла се намира в fonts папката'",
            "Увери се, че си commit-нал roboto font файла в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.be.true;
    });
  });
});
