const CustomError = require("../support/errors/CustomError");

context("task-76", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should not be disabled at first", () => {
    cy.react("App")
      .find("button")
      .then(($button) => {
        expect(
          $button,
          new CustomError({
            issue: "Бутонът не е disabled в началото",
            tips: ["Увери се, че disabled атрибута зависи от стейта"],
          })
        ).to.have.prop("disabled", false);
      });
  });

  it("should have the initial text from the boilerplate", () => {
    cy.react("App")
      .find("button")
      .invoke("text")
      .then((text) => {
        expect(
          text,
          new CustomError({
            issue: "Бутонът не съдържа текстът по подразбиране",
            tips: ["Увери се, че използваш boilerplate-а по условие"],
          })
        ).to.equal("Mark Complete");
      });
  });

  it("should start loading on click", () => {
    cy.react("App")
      .find("button")
      .click()
      .then(($button) => {
        expect(
          $button,
          new CustomError({
            issue: "Бутонът не индикира зареждане",
            tips: [
              "Увери се, че бутонът има клас is-loading, когато трябва да индикира зареждане",
              "Увери се, че бутонът не може да се клика, докато се индикира зареждане",
            ],
          })
        )
          .to.have.class("is-loading")
          .and.to.have.prop("disabled", true);
      });
  });

  it("should be able to add button icon", () => {
    cy.react("App")
      .find("button i")
      .then(($i) => {
        expect(
          $i,
          new CustomError({
            issue: "Когато бутонът не индикира зареждане, няма иконка",
            tips: [
              "Увери се, че показваш иконка чрез i елемент, когато не се индикира зареждане",
            ],
          })
        ).to.exist;

        expect(
          $i,
          new CustomError({
            issue: "Не се показва правилната иконка",
            tips: ["Увери се, че класът на иконката е 'fa-check'"],
          })
        ).to.have.class("fa-check");
      });
  });

  it("should be able to change button text", () => {
    cy.react("App")
      .find("button")
      .then(($button) => {
        expect(
          $button.text(),
          new CustomError({
            issue:
              "Текстът на бутона не се сменя, когато не индикира вече зареждане",
            tips: [],
          })
        ).to.equal("Complete");

        expect(
          $button,
          new CustomError({
            issue: "",
            tips: [],
          })
        ).to.have.prop("disabled", true);
      });
  });
});
