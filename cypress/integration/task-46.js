const CustomError = require("../support/errors/CustomError");

context("task-46", () => {
  it("should have a link tag with a href containing 'https://use.typekit.net/'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('link[href*="https://use.typekit.net/"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага към adobe fonts",
          tips: [
            "Увери се, че си добавил link tag с href атрибут, който съдържа 'https://use.typekit.net/'",
            "Увери се, че си добавил link tag с rel атрибут 'stylesheet'",
          ],
        })
      ).to.have.attr("rel", "stylesheet");
    });
  });

  it("should have html element with the font family set to 'futura-pt, sans-serif'", () => {
    cy.get("html").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Не е зададен font-family на html елемента",
          tips: [
            "Увери се, че си задал font-family property-то на html елемента",
            "Увери се, че зададеното font-family на html елемента е 'futura-pt, sans-serif'",
          ],
        })
      ).to.have.css("font-family", "futura-pt, sans-serif");
    });
  });
});
