const CustomError = require("../support/errors/CustomError");

context("task-59", () => {
  it("Should have discounts which are hidden on bigger resolutions", () => {
    cy.visit(Cypress.config("url"));

    cy.get(".discounts").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Секцията с отстъпките е видима",
          tips: [
            "Увери се, че съществува div с class 'discounts'",
            "Увери се, .discounts е скрито на резолюции над 600px",
          ],
        })
      ).to.be.hidden;
    });
  });

  it("Should have discounts which are visible on mobile", () => {
    cy.viewport(600, 1200);

    cy.get(".discounts").then((el) => {
      expect(
        el,
        new CustomError({
          issue:
            "Секцията с отстъпките не видима на резолюции под 600px ширина",
          tips: [
            "Увери се, че съществува div с class 'discounts'",
            "Увери се, .discounts е показано на резолюции под 600px ширина",
          ],
        })
      ).to.be.visible;
    });
  });
});
