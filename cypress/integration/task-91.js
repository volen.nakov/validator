import { findDeep } from "../support/utils";

const CustomError = require("../support/errors/CustomError");

context("task-91", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have JS app", () => {
    cy.getJsApp(new CustomError(CustomError.common.JS_APP_NOT_FOUNT)).then(
      (app) => expect(app).to.exist
    );
  });

  it(`should have an Application with a _beat instance`, () => {
    cy.getJsApp().then((app) => {
      expect(
        app._beat,
        new CustomError({
          issue: `Липсва _beat инстанцията закачена за Application`,
          tips: [
            `Увери се, си инстанцирал Beat класа в Application`,
            `Увери се, че си закачил инстанцията за класа "this._beat = ..."`,
          ],
        })
      ).to.exist;
    });
  });

  it(`should have the Beat emit the Beat.events.BIT event`, () => {
    cy.getJsApp().then((app) => {
      cy.wrap(app._beat).onEvent("bit");
    });
  });

  it(`should have the _create method defined in the Application class`, () => {
    cy.getJsApp().then((app) => {
      expect(
        app._create,
        new CustomError({
          issue: `Липсва _create метода в Application`,
          tips: [
            `Увери се, си дефинирал метода _create на Application класа`,
            `Увери се, че си изписал името на метода правилно`,
          ],
        })
      ).to.be.a("function");
    });
  });

  it(`should messages for all lyrics`, () => {
    cy.getJsApp().then((app) => {
      const lyrics = ["Ah", "ha", "ha", "ha", "stayin' alive", "stayin' alive"];

      lyrics.forEach((lyric, index) => {
        cy.wrap(app._beat).onEvent("bit");

        cy.get(".message")
          .eq(index)
          .then((el) => {
            const element = el.get(0);

            expect(
              element.innerText,
              new CustomError({
                issue: `Липсва .message елемент за някоя от лириките`,
                tips: [
                  `Увери се, че създаваш .message за всяка една от лириките`,
                  `Увери се, че създаваш лириките последователно след всеки BIT евент`,
                ],
              })
            ).to.eq(lyrics[index]);
          });
      });
    });
  });

  it(`Should have a listener for Beat.events.BIT in the constructor of Application.js`, () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            name: "constructor",
            type: "Identifier",
          },
        });

        const onListener = findDeep(node, {
          type: "CallExpression",
          callee: {
            type: "MemberExpression",
            computed: false,
            object: {
              type: "MemberExpression",
              computed: false,
              object: {
                type: "ThisExpression",
              },
              property: {
                type: "Identifier",
                name: "_beat",
              },
            },
            property: {
              type: "Identifier",
              name: "on",
            },
          },
        });

        const addListener = findDeep(node, {
          type: "CallExpression",
          callee: {
            type: "MemberExpression",
            computed: false,
            object: {
              type: "MemberExpression",
              computed: false,
              object: {
                type: "ThisExpression",
              },
              property: {
                type: "Identifier",
                name: "_beat",
              },
            },
            property: {
              type: "Identifier",
              name: "addListener",
            },
          },
        });

        const hasListener = (onListener || addListener)?.deeper({
          property: {
            name: "BIT",
            type: "Identifier",
          },
        });

        expect(
          hasListener,
          new CustomError({
            issue: `Не намираме закачен listener на _beat инстанцията за event Beat.events.BIT`,
            tips: [
              `Увери се, че си закачил event-a Beat.events.BIT за this._beat`,
              `Увери се, че си закачил event-a правилно https://github.com/primus/eventemitter3`,
              `Увери се, че listener-a се намира в конструктора на Application класа`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should import eventemitter3 in Beat `, () => {
    cy.task("readJSFileSync", { file: "src/js/Beat.js" }).then((data) => {
      const node = findDeep(data, {
        source: {
          type: "Literal",
          value: "eventemitter3",
          raw: '"eventemitter3"',
        },
        type: "ImportDeclaration",
      });

      expect(
        node,
        new CustomError({
          issue: `Не намираме import statement за eventemitter3`,
          tips: [
            `Увери се, че си import-нал eventemitter3 с import https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import`,
            `Увери се, че си import-нал правилния модул`,
          ],
        })
      ).to.exist;
    });
  });

  it(`Should have a Beat class which extends EventEmitter `, () => {
    cy.task("readJSFileSync", { file: "src/js/Beat.js" }).then((data) => {
      const node = findDeep(data, {
        superClass: {
          name: "EventEmitter",
          type: "Identifier",
        },
      }).deeper({
        id: {
          name: "Beat",
          type: "Identifier",
        },
      });

      cy.log(node);

      expect(
        node,
        new CustomError({
          issue: `Изглежда, че Beat не extend-ва EventEmitter`,
          tips: [
            `Увери се, че import-a е наименуван на EventEmitter`,
            `Увери се, че extend-ваш класа с правилно https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/extends`,
          ],
        })
      ).to.exist;
    });
  });
});
