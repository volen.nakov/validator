const CustomError = require("../support/errors/CustomError");

context("task-42", () => {
  it("should have an apple touch icon", () => {
    cy.visit(Cypress.config("url"));

    cy.get('link[rel="apple-touch-icon"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага за favicon за iOS",
          tips: [
            "Увери се, че си добавил link tag с rel атрибут 'apple-touch-icon'",
            "Увери се, че си добавил link tag с href атрибут 'images/apple-touch-icon.png'",
          ],
        })
      ).to.have.attr("href", "images/apple-touch-icon.png");
    });
  });

  it("should have an icon for the 32x32 size", () => {
    cy.get('link[rel="icon"][sizes="32x32"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага за иконка с размер 32x32",
          tips: [
            "Увери се, че си добавил link tag с rel атрибут 'icon'",
            "Увери се, че си добавил link tag с sizes атрибут '32x32'",
          ],
        })
      ).to.have.attr("href", "images/favicon-32x32.png");
    });
  });

  it("should have an icon for the 16x16 size", () => {
    cy.get('link[rel="icon"][sizes="16x16"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага за иконка с размер 16x16",
          tips: [
            "Увери се, че си добавил link tag с rel атрибут 'icon'",
            "Увери се, че си добавил link tag с sizes атрибут '16x16'",
          ],
        })
      ).to.have.attr("href", "images/favicon-16x16.png");
    });
  });

  it("should have the default favicon", () => {
    cy.get('link[rel="icon"]:not([sizes])').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага за favicon",
          tips: [
            "Няма нужда да добавяш допълнителен favicon таг. Проектът е настроен да го прави автоматично",
            "Увери се, че си import-нал правилния проект",
          ],
        })
      ).to.have.attr("href", "favicon.ico");
    });
  });

  it("should have the src/images/favicon.ico file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      expect(
        data,
        new CustomError({
          issue: "Липсва src/images/favicon.ico в branch master",
          tips: [
            "Увери се, че си commit-нал файла src/images/favicon.ico в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.include("src/images/favicon.ico");
    });
  });

  it("should have the src/images/favicon-32x32.png file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      expect(
        data,
        new CustomError({
          issue: "Липсва src/images/favicon-32x32.png в branch master",
          tips: [
            "Увери се, че си commit-нал файла src/images/favicon-32x32.png в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.include("src/images/favicon-32x32.png");
    });
  });

  it("should have the src/images/favicon-16x16.png file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      expect(
        data,
        new CustomError({
          issue: "Липсва src/images/favicon-16x16.png в branch master",
          tips: [
            "Увери се, че си commit-нал файла src/images/favicon-16x16.png в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.include("src/images/favicon-16x16.png");
    });
  });

  it("should have the src/images/apple-touch-icon.png file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      expect(
        data,
        new CustomError({
          issue: "Липсва src/images/apple-touch-icon.png в branch master",
          tips: [
            "Увери се, че си commit-нал файла src/images/apple-touch-icon.png в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.include("src/images/apple-touch-icon.png");
    });
  });

  it("should have the src/images/android-chrome-192x192.png file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      expect(
        data,
        new CustomError({
          issue: "Липсва src/images/android-chrome-192x192.png в branch master",
          tips: [
            "Увери се, че си commit-нал файла src/images/android-chrome-192x192.png в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.include("src/images/android-chrome-192x192.png");
    });
  });

  it("should have the src/images/android-chrome-512x512.png file", () => {
    cy.task("gitListFiles", { ref: "origin/master" }).then((data) => {
      cy.log("Git Files", data);

      expect(
        data,
        new CustomError({
          issue: "Липсва src/images/android-chrome-512x512.png в branch master",
          tips: [
            "Увери се, че си commit-нал файла src/images/android-chrome-512x512.png в branch dev",
            "Увери се, че си merge-нал dev в master",
            "Увери се, че си push-нал промените си",
          ],
        })
      ).to.include("src/images/android-chrome-512x512.png");
    });
  });
});
