const CustomError = require("../support/errors/CustomError");

context("task-98", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should have a LoginForm.module.css file", () => {
    cy.task("readFileSync", {
      file: "src/components/LoginForm.module.css",
    }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Не съществува файл LoginForm.module.css",
          tips: ["Увери се, че превръщаш файла LoginForm.css в css module"],
        })
      ).to.exist;
    });
  });

  it("should have the LoginForm component imported and used in the App.js", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue:
            "LoginForm компонентът не е импортнат и/или използван в App.js компонента",
          tips: [
            "Увери се, че импортваш LoginForm компонента в App.js",
            "Увери се, че използваш LoginForm компонента в JSX-a на App.js",
          ],
        })
      ).to.include("<LoginForm");
    });
  });

  it("should have a form with labels and inputs in the LoginForm component", () => {
    cy.task("reactMount", {
      name: "LoginForm",
      path: "src/components/LoginForm.js",
    }).then(() => {
      cy.reload();
      cy.waitForReact();
      cy.react("LoginForm").then((form) => {
        expect(
          form,
          new CustomError({
            issue: "Няма рендериран LoginForm компонент",
            tips: ["Увери се, че няма проблеми при рендерирането на LoginForm"],
          })
        ).to.exist;
      });

      cy.get("form").then(($form) => {
        expect(
          $form,
          new CustomError({
            issue: "Няма създаден form element",
            tips: ["Увери се, че си създал form element"],
          })
        ).to.exist;
      });
    });

    cy.get("label").then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Няма 2 лейбъл-а за 2-та инпута",
          tips: ["Увери се, че си създал label за всеки input"],
        })
      ).to.have.length(2);
    });

    cy.get("input").then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Няма 2 inputs",
          tips: [
            "Увери се, че си създал 2 input полета - за username и за password",
          ],
        })
      ).to.have.length(2);
    });

    cy.get("button").then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Няма създаден бутон",
          tips: ["Увери се, че си създал submit button"],
        })
      ).to.have.length(1);
    });
  });
});
