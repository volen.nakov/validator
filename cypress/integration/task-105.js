const CustomError = require("../support/errors/CustomError");
const { findDeep } = require("../support/utils");

context("task-105", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  it("should have a logo component which can be mounted", () => {
    cy.task("nextMount", {
      name: "Logo",
      path: "src/components/logo/Logo.jsx",
    });
    cy.visit(Cypress.config("url"));
    cy.waitForReact(1000, "#__next");
  });

  it("should have a logo component which accepts a 'type' prop", () => {
    cy.task("readJSFileSync", {
      file: "src/components/logo/Logo.jsx",
      jsx: true,
    }).then((data) => {
      const params = findDeep(data, {
        type: "FunctionDeclaration",
      })?.params;
      const found = findDeep(params, {
        key: {
          name: "type",
          type: "Identifier",
        },
      });

      expect(
        found,
        new CustomError({
          issue: "Няма дефиниран type prop на Logo компонента",
          tips: [
            "Увери се, че Logo е function component",
            "Увери се, че компонентът приема prop с име type",
            "Увери се, че компонентът приема prop във формата { type }",
          ],
        })
      ).to.exist;
    });
  });

  it("should have a logo component which shows the default image when no 'type' prop is set", () => {
    cy.react("Logo").then((comp) => {
      cy.wrap(comp)
        .get("img")
        .then((el) => {
          expect(
            el.get(0).src,
            new CustomError({
              issue: "Не се визуализира правилната картинка за логото",
              tips: [
                "Увери се, че, когато няма подаден prop 'type' се визуализира логото в нормално състояние",
                "Увери се, че използваш svg-то, което е предоставено като asset за проекта",
                "Увери се, че използваш обикновен <img> таг",
              ],
            })
          ).to.contain("/images/logo.svg");
        });
    });
  });

  it("should have a logo component which shows the 'muted' image when the 'type' prop is set to 'muted'", () => {
    cy.task("nextMount", {
      name: "Logo",
      props: {
        type: "muted",
      },
      path: "src/components/logo/Logo.jsx",
    });
    cy.visit(Cypress.config("url"));
    cy.waitForReact(1000, "#__next");
    cy.react("Logo", { options: { timeout: 100000 } }).then((comp) => {
      cy.wrap(comp)
        .get("img")
        .then((el) => {
          expect(
            el.get(0).src,
            new CustomError({
              issue: "Не се визуализира правилната картинка за логото",
              tips: [
                "Увери се, че, когато няма подаден prop 'type' се визуализира логото в нормално състояние",
                "Увери се, че използваш svg-то, което е предоставено като asset за проекта",
                "Увери се, че използваш обикновен <img> таг",
              ],
            })
          ).to.contain("/images/logo-muted.svg");
        });
    });
  });
});
