const CustomError = require("../support/errors/CustomError");

context("task-51", () => {
  it("Should have an input with a name 'name' and a type 'text'", () => {
    cy.visit(Cypress.config("url"));

    cy.get(".card").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Някоя от картите не е видима",
          tips: [
            "Увери се, че всички карти са видими при първоначално зареждане на страницата",
          ],
        })
      ).to.be.visible;
    });
  });

  it("Should have an input with a name 'name' and a type 'text'", () => {
    cy.wait(5000);

    cy.get(".card:not(.active)").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Има видима неактивна карта",
          tips: [
            "Увери се, че всички карти, които нямат class 'active' не са видими 3 секунди след зареждане на страницата",
          ],
        })
      ).to.not.be.visible;
    });
  });
});
