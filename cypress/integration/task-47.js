const CustomError = require("../support/errors/CustomError");

context("task-47", () => {
  it("Should have an @import rule in src/scss/base/_fonts.scss", () => {
    cy.visit(Cypress.config("url"));

    cy.task("readFileSync", { file: "src/scss/base/_fonts.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue: "Шрифтът не е import-нат в _fonts.scss",
            tips: [
              "Увери се, че шрифта е import-нат чрез @import",
              "Увери се, че шрифта е import-нат в src/scss/base/_fonts.scss",
            ],
          })
        ).to.include(`@import url("https://use.typekit.net/`);
      }
    );
  });

  it("should have html element with the font family set to 'futura-pt, sans-serif'", () => {
    cy.get("html").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Не е зададен font-family на html елемента",
          tips: [
            "Увери се, че си задал font-family property-то на html елемента",
            "Увери се, че зададеното font-family на html елемента е 'futura-pt, sans-serif'",
          ],
        })
      ).to.have.css("font-family", "futura-pt, sans-serif");
    });
  });
});
