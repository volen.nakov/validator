import {
  testProps,
  testPropsDefaults,
  testSCSS,
} from "../support/utils/react/tests";

import CustomError from "../support/errors/CustomError";
import { findDeep } from "../support/utils";
import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "Featured",
  path: "src/components/featured/Featured.jsx",
  props: {
    items: [
      {
        image:
          "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e?w=500&h=500",
        title: "Breakfast",
        rows: 2,
        cols: 3,
        href: "/about",
      },
      {
        image:
          "https://images.unsplash.com/photo-1551782450-a2132b4ba21d?w=500&h=500",
        title: "Burger",
        href: "/about",
      },
      {
        image:
          "https://images.unsplash.com/photo-1522770179533-24471fcdba45?w=500&h=500",
        title: "Camera",
        href: "/about",
      },
      {
        image:
          "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c?w=500&h=500",
        title: "Coffee",
        href: "/about",
      },
      {
        image:
          "https://images.unsplash.com/photo-1533827432537-70133748f5c8?w=500&h=500",
        title: "Hats",
        href: "/about",
      },
      {
        image:
          "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62?w=500&h=500",
        title: "Honey",
        href: "/about",
      },
      {
        image:
          "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6?w=500&h=500",
        title: "Basketball",
        href: "/about",
      },
    ],
  },
};

const css = {
  path: "src/components/featured/Featured.module.scss",
};

context("task-110", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["items"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "items",
        default: [],
      },
    ],
  });

  testSCSS({
    ...css,
  });

  it(`should have used the Container component`, () => {
    mount({ ...component });
    cy.react("MuiContainerRoot");
  });

  it(`should have used the ImageList component`, () => {
    mount({ ...component });
    cy.react("MuiImageListRoot");
  });

  it(`should have used the ImageListItem component`, () => {
    mount({ ...component });
    cy.react("MuiImageListItemRoot");
  });

  component.props.items.forEach((item, index) => {
    it(`should have rendered image number ${index} with a src of ${item.image}`, () => {
      mount({ ...component });
      cy.get("img").then((el) => {
        expect(
          el.get(index)?.src,
          new CustomError({
            issue: `Не се е изрендирала правилната картинка на компонента`,
            tips: [
              `Увери се, че циклираш елементите от items масива и създаваш ImageListItem компоненти`,
              `Увери се, че подаваш правилния път към картинката`,
            ],
          })
        ).to.eq(item.image);
      });
    });
  });

  it(`should have the first image bigger than the others`, () => {
    mount({ ...component });
    cy.get(".MuiImageListItem-root").then((el) => {
      expect(
        el.get(0)?.getAttribute("style"),
        new CustomError({
          issue: `Не се е изрендирала правилната картинка на компонента`,
          tips: [
            `Увери се, че циклираш елементите от items масива и създаваш ImageListItem компоненти`,
            `Увери се, че подаваш правилния път към картинката`,
          ],
        })
      ).to.include("grid-column-end:span 3;grid-row-end:span 2");
    });
  });

  it(`should not render the items if no prop items are passed`, () => {
    const noProps = {
      ...component,
      props: {},
    };

    mount({ ...noProps });
    cy.get(".MuiImageList-root").then((el) => {
      expect(
        el.get(0)?.childElementCount,
        new CustomError({
          issue: `Създават се елементи, когато няма подадени props items`,
          tips: [
            `Увери се, че prop-овете ти са динамични`,
            `Увери се, че че използваш ImageList компонента`,
          ],
        })
      ).to.eq(0);
    });
  });

  it(`should have used useRouter`, () => {
    cy.task("readJSFileSync", { file: component.path, jsx: true }).then(
      (data) => {
        const result = findDeep(data, {
          callee: {
            name: "useRouter",
            type: "Identifier",
          },
        });

        expect(
          result,
          new CustomError({
            issue: `не е използван useRouter hook-a, за смяната на страниците при клик на item`,
            tips: [
              `Увери се, че използваш useRouter от nextjs`,
              `Увери се, че използваш router.push...`,
            ],
          })
        ).to.exist;
      }
    );
  });
});
