const CustomError = require("../support/errors/CustomError");

context("task-44", () => {
  it("should have a link tag with a href 'https://fonts.googleapis.com'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('link[href="https://fonts.googleapis.com"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага към google fonts",
          tips: [
            "Увери се, че си добавил link tag с href атрибут 'https://fonts.googleapis.com'",
            "Увери се, че си добавил link tag с rel атрибут 'preconnect'",
          ],
        })
      ).to.have.attr("rel", "preconnect");
    });
  });

  it("should have a link tag with a href 'https://fonts.gstatic.com'", () => {
    cy.get('link[href="https://fonts.gstatic.com"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага към google fonts",
          tips: [
            "Увери се, че си добавил link tag с href атрибут 'https://fonts.gstatic.com'",
            "Увери се, че си добавил link tag с rel атрибут 'preconnect'",
          ],
        })
      ).to.have.attr("rel", "preconnect");
    });
  });

  it("should have a link tag with a href containing 'https://fonts.googleapis.com/css2?family=Roboto'", () => {
    cy.get(
      'link[href*="https://fonts.googleapis.com/css2?family=Roboto"]'
    ).then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва link тага към google fonts",
          tips: [
            "Увери се, че си добавил link tag с href атрибут 'https://fonts.gstatic.com'",
            "Увери се, че си добавил link tag с rel атрибут 'stylesheet'",
          ],
        })
      ).to.have.attr("rel", "stylesheet");
    });
  });

  it("should have html element with the font family set to 'Roboto, sans-serif'", () => {
    cy.get("html").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Не е зададен font-family на html елемента",
          tips: [
            "Увери се, че си задал font-family property-то на html елемента",
            "Увери се, че зададеното font-family на html елемента е 'Roboto, sans-serif'",
          ],
        })
      ).to.have.css("font-family", "Roboto, sans-serif");
    });
  });
});
