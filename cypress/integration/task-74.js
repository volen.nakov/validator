const CustomError = require("../support/errors/CustomError");

context("task-74", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should have one state variable and one computation based on it", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content.match(/useState/g),
        new CustomError({
          issue: "Не се използва useState",
          tips: [
            "Увери се, че използваш useState, за да пазиш стойността от текстовото поле",
          ],
        })
      ).to.have.length(2);

      expect(
        content.match(/useMemo/g),
        new CustomError({
          issue: "Не се използва useMemo",
          tips: [
            "Увери се, че използваш useMemo, за да проверяваш дали числото в полето е валидно",
          ],
        })
      ).to.have.length(2);
    });
  });

  it("should use the useMemo hook and have 1 dependency", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "text стейт променливата не е dependency на useMemo",
          tips: [
            "Увери се, че изчислението в useMemo зависи от текущата на текстовото поле",
          ],
        })
      ).to.contain("[text]");
    });
  });

  it("should use an onChange handler for the input", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Не се използва onChange handler при инпута",
          tips: [
            "Добави onChange hanlder при input елемента, за да обновяваш text стейт променливата чрез него",
          ],
        })
      ).to.contain("onChange={");
    });
  });

  it("should have a styled text field", () => {
    cy.react("App")
      .find("input")
      .then(($input) => {
        expect(
          $input,
          new CustomError({
            issue: "Текстовото поле няма необходимите по условие класове",
            tips: ["Увери се, че използваш boilerplate-а на задачата"],
          })
        )
          .to.have.class("input")
          .and.to.have.class("is-large");

        expect(
          $input,
          new CustomError({
            issue: "input елементът не е от текстов тип",
            tips: ["Увери се, че използваш boilerplate-а на задачата"],
          })
        ).to.have.prop("type", "text");
      });
  });

  it("should have an icon on the right", () => {
    cy.react("App")
      .find("input")
      .invoke("next")
      .then(($span) => {
        expect(
          $span,
          new CustomError({
            issue: "Липсва иконка от дясната страна на полето",
            tips: ["Увери се, че използваш boilerplate-а на задачата"],
          })
        )
          .to.have.class("icon")
          .and.to.have.class("is-small")
          .and.to.have.class("is-right");
      });
  });

  it("should have a FontAwesome icon class in the span", () => {
    cy.react("App")
      .find("span.icon.is-small.is-right")
      .find("i")
      .invoke("attr", "class")
      .then((classList) => {
        expect(
          classList,
          new CustomError({
            issue: "Иконката няма необходимите класове от FontAwesome",
            tips: ["Увери се, че използваш boilerplate-а на задачата"],
          })
        )
          .to.contain("fas")
          .and.to.contain("fa-");
      });
  });

  it("should be able to type in the text field", () => {
    const stringToType = "the text field works";
    cy.react("App")
      .find("input")
      .type(stringToType)
      .then(($input) => {
        expect(
          $input,
          new CustomError({
            issue:
              "Стойността на текстовото поле не се отразява във value атрибута",
            tips: [
              "Увери се, че value атрибута на текстовото поле се задава от text стейта",
            ],
          })
        ).to.have.prop("value", stringToType);
      });
  });

  it("should update the icon when new data is typed in", () => {
    cy.react("App")
      .find("input")
      .clear()
      .type("12")
      .invoke("next")
      .find("i")
      .then(($i) => {
        expect(
          $i,
          new CustomErro({
            issue: "Иконката не се променя, когато е въведено валидно число",
            tips: [
              "Увери се, че когато числото в полето е валидно, елементът с иконката има клас fa-check",
            ],
          })
        ).to.have.class("fa-check");
      });

    cy.react("App")
      .find("input")
      .clear()
      .type("12a")
      .invoke("next")
      .find("i")
      .then(($i) => {
        expect(
          $i,
          new CustomError({
            issue: "Иконката не се променя, когато е въведено невалидно число",
            tips: [
              "Увери се, че когато числото в полето е невалидно, елементът с иконката има клас fa-times",
            ],
          })
        ).to.have.class("fa-times");
      });

    cy.react("App")
      .find("input")
      .clear()
      .type("a12")
      .invoke("next")
      .find("i")
      .then(($i) => {
        expect(
          $i,
          new CustomError({
            issue: "Иконката не се променя, когато е въведено невалидно число",
            tips: [
              "Увери се, че когато числото в полето е невалидно, елементът с иконката има клас fa-times",
            ],
          })
        ).to.have.class("fa-times");
      });
  });
});
