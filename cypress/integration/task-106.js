import CustomError from "../support/errors/CustomError";
import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "Header",
  path: "src/components/header/Header.jsx",
};

const css = {
  path: "src/components/header/Header.module.scss",
};

context("task-106", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  it(`should have used the Logo component`, () => {
    mount({ ...component });
    cy.react("Logo");
  });

  it("should import Search icon from @mui/icons-material", () => {
    mount({ ...component });
    cy.react("Header").then((comp) => {
      cy.wrap(comp)
        .get("[data-testid=SearchIcon]")
        .then((el) => {
          expect(el).to.exist;
        });
    });
  });

  it(`should have used Grid from MUI`, () => {
    mount({ ...component });

    cy.react("Header").then((comp) => {
      cy.wrap(comp)
        .get("[class*=MuiGrid]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: `Не е използван Grid от Material UI`,
              tips: [
                `Увери се, че използваш Grid според условието.`,
                `Увери се, че използваш Grid container и Grid item.`,
              ],
            })
          ).to.exist;
        });
    });
  });

  it(`should have used 3 Grid items from MUI`, () => {
    mount({ ...component });

    cy.react("Header").then((comp) => {
      cy.wrap(comp)
        .get("[class*=MuiGrid-item]")
        .then((el) => {
          expect(
            el,
            new CustomError({
              issue: `Не са използвани 3 Grid items от Material UI`,
              tips: [
                `Увери се, че използваш Grid items според условието.`,
                `Увери се, че използваш 3 Grid items - за логото, за search bar-a и за трите бутона`,
              ],
            })
          ).to.have.length(3);
        });
    });
  });

  it(`should gave a ${css.path} css module`, () => {
    cy.task("readFileSync", {
      file: css.path,
    });
  });

  it(`should have а used the colors variables`, () => {
    cy.task("readFileSync", {
      file: css.path,
    }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: `Не се използват variable-ите за цветовете в от styles/colors`,
          tips: [
            `Увери се, че import-ваш файла използвайки "@use"`,
            `Увери се, че import-ваш правилния файл`,
          ],
        })
      ).to.include(`@use "../../styles/colors`);
    });
  });
});
