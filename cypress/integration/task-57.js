const CustomError = require("../support/errors/CustomError");

context("task-57", () => {
  it("should have a div element with a class chat", () => {
    cy.visit(Cypress.config("url"));
    cy.get("div.chat").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Липсва div с клас chat",
          tips: [
            "Увери се, че си създал div html element",
            "Увери се, че си добавил клас chat към div елемента",
          ],
        })
      ).to.exist;
    });
  });
  it("should contain a position property", () => {
    cy.get(".chat").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Не е използвано position property-то",
          tips: [
            "Увери се, че използваш position property-то за позициониране на елемента",
            "Увери се, че си добавил 'fixed' стойност към position property-то",
          ],
        })
      ).to.have.css("position", "fixed");
    });
  });
  it("should have bottom and right css properties", () => {
    cy.get(".chat").then(($div) => {
      expect($div, new CustomError())
        .to.have.css("bottom", "0px")
        .and.to.have.css("right", "0px");
    });
  });
});
