const CustomError = require("../support/errors/CustomError");

context("task-99", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have the styled-components package installed and added in package.json", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.["styled-components"],
        new CustomError({
          issue: "Styled-components пакетът не е инсталиран",
          tips: [
            "Увери се, че си прочел условието на задачата внимателно и си инсталирал styled-components",
            "Може да използваш командата npm install <package_name>",
          ],
        })
      ).to.exist;
    });
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should be using styled components in the form.js file", () => {
    cy.task("readFileSync", { file: "src/components/form.js" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е създаден div елемент със styled-components",
          tips: [
            "Увери се, че създаваш div със styled-components правилно. Може да отвориш документацията на библиотеката.",
          ],
        })
      ).to.include("styled.div`");

      expect(
        data,
        new CustomError({
          issue: "Не е създаден input елемент със styled-components",
          tips: [
            "Увери се, че създаваш input със styled-components правилно. Може да отвориш документацията на библиотеката.",
          ],
        })
      ).to.include("styled.input`");

      expect(
        data,
        new CustomError({
          issue: "Не е създаден button елемент със styled-components",
          tips: [
            "Увери се, че създаваш button със styled-components правилно. Може да отвориш документацията на библиотеката.",
          ],
        })
      ).to.include("styled.button`");
    });
  });

  it("should export the styled components from the form.js file", () => {
    cy.task("readFileSync", { file: "src/components/form.js" }).then((data) => {
      const nodes = data.split("export");
      const hasOneExport = nodes.length === 2;
      const hasFourExports = nodes.length === 5;

      expect(
        hasOneExport || hasFourExports,
        new CustomError({
          issue: "Не са експортнати styled components от form.js",
          tips: [
            "Увери се, че експортваш създадените компоненти, защото иначе няма да можеш да ги ползваш в други компоненти",
          ],
        })
      ).to.be.true;
    });
  });
});
