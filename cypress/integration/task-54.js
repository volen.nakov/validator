const CustomError = require("../support/errors/CustomError");

context("task-54", () => {
  it("should have a div with a class name container", () => {
    cy.visit(Cypress.config("url"));
    cy.get(".container")
      .then(($div) => {
        expect(
          $div,
          new CustomError({
            issue: "Не съществува div с клас container",
            tips: ["Увери се, че имаш div, който има сетнат клас container"],
          })
        ).to.exist
      });
  });
  it("should have a display property of grid", () => {
    cy.get(".container").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Div-ът с клас container няма display проперти grid",
          tips: [
            "Увери се, че си сетнал display проперти grid на div-a с клас container",
          ],
        })
      ).to.have.css("display", "grid");
    });
  });
  it("should have 3 items per row", () => {
    cy.get(".container").then(($div) => {
      expect(
        $div,
        new CustomError({
          issue: "Трябва да има 3 grid items на ред",
          tips: ["Увери се, че си използвал property-то grid-template-columns"],
        })
      ).to.have.css("grid-template-columns");
    });
  });
});
