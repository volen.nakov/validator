const CustomError = require("../support/errors/CustomError");

context("task-73", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should increment count in the button text", () => {
    const times = Array.from({ length: 50 }, (_, i) => i + 1);

    cy.get("button")
      .first()
      .then(($button) => {
        times.forEach((i) => {
          $button.click();
          cy.title((title) => {
            expect(
              $button,
              new CustomError({
                issue: "Текстът на бутона не се променя",
                tips: [
                  "Увери се, че използваш expression в бутона, който показва текущата стойност на брояча.",
                  "Увери се, че спазваш показваш брояча точно както е по условие",
                ],
              })
            ).to.have.text(`Count (${i + 1})`);

            expect(
              title,
              new CustomError({
                issue: "Заглавието не е синхронизирано със стейта",
                tips: [
                  "Увери се, че използваш правилните dependency-та в useEffect",
                  "Увери се, че спазваш показваш брояча точно както е по условие",
                ],
              })
            ).to.have.text(`Count (${i + 1})`);
          });
        });
      });
  });

  it("should update the document title with useEffect in the App component", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Не е използван useEffect",
          tips: [
            "Увери се, че променяш заглавието на страницата чрез useEffect hook",
          ],
        })
      ).to.include("useEffect");

      expect(
        content,
        new CustomError({
          issue: "Не е използван document.title",
          tips: [
            "Правилният начин за промяна на заглавието на страницата е чрез document.title",
            "Увери се, че използваш document.title в кода си",
          ],
        })
      ).to.include("document.title");
    });
  });

  it("should manage the App component state with useState", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Не използваш useState hook",
          tips: [
            "Необходимо е да пазиш някъде в екосистемата на React конкретния стейт на брояча",
          ],
        })
      ).to.include("useState");
    });
  });
});
