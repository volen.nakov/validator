const CustomError = require("../support/errors/CustomError");

context("task-85", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have app.js file", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "app.js файлът не съществува",
          tips: ["Увери се, че не си преименувал app.js файла"],
        })
      ).to.exist;
    });
  });

  it("should use fetch API", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      cy.log(data);
      expect(
        data,
        new CustomError({
          issue: "Не е използван fetch API",
          tips: [
            "Увери се, че използваш fetch API и правиш заявката чрез него",
          ],
        })
      ).to.include("fetch(");
    });
  });

  it("should have a limit of 10 Pokemons", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      cy.log(data);
      expect(
        data,
        new CustomError({
          issue: "Не се fetch-ват 10 Pokemons",
          tips: [
            "Увери се, че използваш API-то по условие",
            "Увери се, че правиш заявката с https",
            "Увери се, че добавяш ?limit=10 на края на url-a",
          ],
        })
      )
        .to.include("https://pokeapi.co/api")
        .and.to.include("?limit=10");
    });
  });

  it("should use document.createElement", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      cy.log(data);
      expect(
        data,
        new CustomError({
          issue: "Не се използва document.createElement",
          tips: [
            "Увери се, че използваш document.createElement за създаването на елементи",
            "Увери се, че добавяш създаваш 'li'",
          ],
        })
      ).to.include("document.createElement");
    });
  });

  it("should create 10 list items", () => {
    cy.get("ul li").then(($li) => {
      expect(
        $li,
        new CustomError({
          issue: "Не са създадени 10 list items",
          tips: [
            "Увери се, че създаваш list item за всеки Pokemon",
            "Увери се, че fetch-ваш 10 Pokemons",
          ],
        })
      ).to.have.length(10);
    });
  });
});
