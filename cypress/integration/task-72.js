const CustomError = require("../support/errors/CustomError");

context("task-72", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should have the required tags on first render", () => {
    const tags = ["boomdotdev", "task", "tags", "react"];

    cy.react("App")
      .find(".tags")
      .then(($tagsContainer) => {
        expect(
          $tagsContainer.children(),
          new CustomError({
            issue: "Рендерираните тагове ги няма или не са 4 на брой",
            tips: [
              "Увери се, че всички тагове по условие са налице в DOM дървото",
            ],
          })
        ).to.have.length(4);
      });

    cy.react("App")
      .find(".tag")
      .then(($tags) => {
        $tags.each((_, $tag) => {
          expect(
            tags.map((tag) => `#${tag}`),
            new CustomError({
              issue:
                "Списъкът от тагове съдържа елементи, които не са очаквани",
              tips: [
                "Увери се, че стойността на title prop-a е точно както в условието",
              ],
            })
          ).to.include($tag.innerText);
        });
      });
  });

  it("should work standalone as separate component", () => {
    const tags = ["boom", "dot", "dev"];

    cy.task("reactMount", {
      name: "Tags",
      path: "src/Tags.js",
      props: {
        tags,
      },
    }).then(() => {
      cy.reload();
      cy.waitForReact();
      cy.react("Tags")
        .find(".tag")
        .then(($tags) => {
          $tags.each((_, $tag) => {
            expect(
              tags.map((tag) => `#${tag}`),
              new CustomError({
                issue: "Tags компонентът не работи извън App компонента",
                tips: [
                  "Увери се, че текстът на всеки таг започва с # отпред",
                  "Увери се, че компонентът ти може да се използва в маркъпа на всеки друг компонент.",
                ],
              })
            ).to.include($tag.innerText);
          });
        });
    });
  });
});
