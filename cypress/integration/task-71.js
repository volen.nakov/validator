const CustomError = require("../support/errors/CustomError");

context("task-71", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered the Document component inside the App", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });

    cy.react("Document", {
      props: {
        title: "Terms and Conditions",
      },
    }).then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "Document",
        })
      ).to.have.length(1);
    });
  });

  it("should have an h1 title matching the title prop", () => {
    cy.react("Document", {
      props: {
        title: "Terms and Conditions",
      },
    })
      .find(".title")
      .invoke("text")
      .then((title) => {
        expect(
          title,
          new CustomError({
            issue: "Prop-овете не съвпадат с дадените в условието",
            tips: [
              "Увери се, че стойността на title prop-a е точно както в условието",
            ],
          })
        ).to.equal("Terms and Conditions");
      });
  });

  // Test wasn't working probably. A good thing will be to replace this test
  // with something equivalent
  // it("should have dynamic content in the Document component", () => {
  //   let content = null;

  //   cy.wait(500);

  //   cy.react("Document")
  //     .find(".content")
  //     .invoke("text")
  //     .then((text) => {
  //       content = text;
  //     });

  //   cy.reload();
  //   cy.waitForReact();

  //   cy.react("Document")
  //     .find(".content")
  //     .invoke("text")
  //     .then((text) => {
  //       expect(
  //         text,
  //         new CustomError({
  //           issue: "Съдържанието в контейнера с клас .content не е динамично",
  //           tips: [
  //             "Увери се, че съдържанието на документа се слага в елемент с клас .content",
  //             "Увери се, че state-ът на Document компонента се обновява спрямо взетото съдържание от заявката",
  //           ],
  //         })
  //       ).to.not.equal(content);
  //     });
  // });

  it("should fetch data with useEffect in App component", () => {
    cy.task("readFileSync", { file: "src/App.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "App компонентът не използва useEffect hook",
          tips: [
            "Увери се, че правиш заявката за текста на документа посредством useEffect",
          ],
        })
      ).to.include("useEffect");

      expect(
        content,
        new CustomError({
          issue: "Не е използвана функцията fetch",
          tips: ["Използвай функцията fetch, за да вземаш данните от сървъра"],
        })
      ).to.include("fetch");

      expect(
        content,
        new CustomError({
          issue:
            "За заявката не се използва даденото по условие API за Lorem Ipsum",
          tips: [
            "Увери се, че адресът на заявката към API-то започва с https",
            "Увери се, че използваш API-то, дадено в условието",
          ],
        })
      ).to.include("https://jaspervdj.be/lorem-markdownum/markdown.txt");

      expect(
        content,
        new CustomError({
          issue: "Не е използвано response.text()",
          tips: [
            "Lorem Ipsum текстът идва в директен текстови формат, а не в JSON",
            "Използвай функцията .text(), която заедно с .json(), е неразделна част от fetch",
          ],
        })
      ).to.include("response.text()");
    });
  });

  it("should not fetch data in Document component", () => {
    cy.task("readFileSync", { file: "src/Document.js" }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: "Използван е useEffect в Document компонента",
          tips: [
            "Ако използваш useEffect за контрол на скролинга – има по-лесен начин",
            "Увери се, че заявката към API-то не се прави вътре в Document компонента",
          ],
        })
      ).to.not.include("useEffect");

      expect(
        content,
        new CustomError({
          issue: "Използва се функцията fetch в Document компонента",
          tips: [
            "Увери се, че заявката към API-то не се прави вътре в Document компонента",
          ],
        })
      ).to.not.include("fetch");

      const requestError = new CustomError({
        issue: "Заявката към API-то се прави в Document компонента",
        tips: [
          "Увери се, че заявката се прави в App компонента, а резултатът от нея се подава като prop на Document",
        ],
      });

      expect(content, requestError).to.not.include(
        "https://jaspervdj.be/lorem-markdownum/markdown.txt"
      );

      expect(content, requestError).to.not.include("response.text()");
    });
  });

  it("should have a disabled button with the given text", async () => {
    cy.react("Document")
      .find("button")
      .then(($button) => {
        expect(
          $button,
          new CustomError({
            issue: "Бутонът не съдържа текста, изискван по условие",
            tips: [
              "Увери се, че текстът на бутона е точно както е зададен в задачата",
            ],
          })
        ).to.have.text("I Agree");

        expect(
          $button,
          new CustomError({
            issue:
              "Бутонът не е disabled при начален рендър на Document компонента",
            tips: ["Увери се, че в началото бутонът има prop 'disabled'"],
          })
        ).to.have.prop("disabled");
      });
  });

  it("should scroll to bottom and enable button", () => {
    cy.get(".content")
      .first()
      .wait(500) // TODO remove wait when we migrate to cypress v6 >. Then we will intercept the fetch request and a wait won't be needed
      .then((el) => {
        const element = el.get(0);
        element.scrollTop = element.scrollHeight;
      })
      .wait(500)
      .then(() => cy.get("button").first())
      .then(($button) => {
        return expect(
          $button,
          new CustomError({
            issue:
              "Бутонът не става активен, когато текстът на документа скролне до долу",
            tips: [
              "Увери се, че handle-ваш промяната на скрола в елемента с клас .content",
              "Увери се, че prop-ът 'disabled' се променя в зависимост от скрола",
            ],
          })
        ).to.have.prop("disabled", false);
      })
      .then(() => {
        return cy
          .get(".content")
          .first()
          .scrollTo("top")
          .wait(500)
          .then(() => cy.get("button").first());
      })
      .then(($button) => {
        return expect(
          $button,
          new CustomError({
            issue: "Бутонът отново става неактивен при скрол нагоре",
            tips: [
              "Увери се, че 'disabled' prop-ът на бутона се изменя само веднъж - когато е скролнато до долу",
            ],
          })
        ).to.have.prop("disabled", false);
      });
  });
});
