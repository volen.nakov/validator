import { findDeep } from "../support/utils";

const CustomError = require("../support/errors/CustomError");
const pizzas = [
  {
    type: "hawaiian",
    price: 8.99,
  },
  {
    type: "pepperoni",
    price: 9.99,
  },
  {
    type: "margherita",
    price: 7.99,
  },
];

context("task-92", () => {
  beforeEach(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have JS app", () => {
    cy.getJsApp(new CustomError(CustomError.common.JS_APP_NOT_FOUNT)).then(
      (app) => expect(app).to.exist
    );
  });

  pizzas.forEach((pizza) => {
    it(`should have a pizza with the type of ${pizza.type}`, () => {
      cy.get(`.card.type-${pizza.type}`).then((el) => {
        expect(
          el,
          new CustomError({
            issue: `Липсва .card от тип ${pizza.type}`,
            tips: [
              `Увери се, че създаваш Card за всеки един от елементите в pizzas`,
              `Увери се, че си import-нал правилното repository`,
            ],
          })
        ).to.exist;
      });
    });
  });

  pizzas.forEach((pizza) => {
    it(`should have a pizza which when clicked creates a notification with a type of ${pizza.type}`, () => {
      cy.get(`.card.type-${pizza.type}`).click();
      cy.get(`.notification.type-${pizza.type}`).then((el) => {
        expect(
          el,
          new CustomError({
            issue: `Липсва .notification от тип ${pizza.type}`,
            tips: [
              `Увери се, че създаваш Notification при click на Card за всеки един от елементите в pizzas`,
              `Увери се, че добавяш клас на .notification, който е type-{type}, където {type} е типа на notification-a`,
            ],
          })
        ).to.exist;
      });
    });
  });

  pizzas.forEach((pizza) => {
    it(`should have a pizza which when clicked creates a notification with a a price of ${pizza.price}`, () => {
      cy.get(`.card.type-${pizza.type}`).click();
      cy.get(`.notification.type-${pizza.type} .price`).then((el) => {
        expect(
          el.get(0).innerText,
          new CustomError({
            issue: `Липсва .notification от тип ${pizza.type}`,
            tips: [
              `Увери се, че създаваш Notification при click на Card за всеки един от елементите в pizzas`,
              `Увери се, че добавяш клас на .notification, който е type-{type}, където {type} е типа на notification-a`,
            ],
          })
        ).to.eq(
          pizza.price.toLocaleString("de-DE", {
            style: "currency",
            currency: "EUR",
          })
        );
      });
    });
  });

  it(`should close the notification when the close button is clicked`, () => {
    cy.get(`.card`).eq(0).click();
    cy.get(`.notification .delete`).click();
    cy.get(`.notification`).should("not.exist");
  });

  it(`should have a notification with the class "is-danger" if the hawaiian pizza is clicked`, () => {
    cy.get(`.card.type-hawaiian`).click();

    cy.get(`.notification.type-hawaiian`).then((el) => {
      expect(
        el.get(0).classList.contains("is-danger"),
        new CustomError({
          issue: `Notification-a от тип hawaiian няма class is-danger`,
          tips: [
            `Увери се, добавяш is-danger класа на елемента с class notification`,
          ],
        })
      ).to.be.true;
    });
  });

  it(`Should import formatCurrency in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          type: "ImportDeclaration",
          specifiers: [
            {
              type: "ImportSpecifier",
              local: {
                type: "Identifier",
                name: "formatCurrency",
              },
              imported: {
                type: "Identifier",
                name: "formatCurrency",
              },
            },
          ],
        });

        expect(
          node,
          new CustomError({
            issue: `Не намираме import statement за formatCurrency в Notification`,
            tips: [
              `Увери се, че си import-нал formatCurrency с import https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import`,
              `Увери се, че си import-нал правилния модул`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should import classNames in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          type: "ImportDeclaration",
          specifiers: [
            {
              type: "ImportDefaultSpecifier",
              local: {
                type: "Identifier",
                name: "classNames",
              },
            },
          ],
        });

        expect(
          node,
          new CustomError({
            issue: `Не намираме import statement за classNames в Notification`,
            tips: [
              `Увери се, че си import-нал classNames с import https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import`,
              `Увери се, че си import-нал правилния модул`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should have a empty method in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          type: "MethodDefinition",
          key: {
            type: "Identifier",
            name: "empty",
          },
          kind: "method",
        });

        expect(
          node,
          new CustomError({
            issue: `Не намираме empty метод в в Notification`,
            tips: [
              `Увери се, че си дефинирал empty metod в Notification`,
              `Увери се, че си изписал името на метода правилно`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should have а type argument in the render method in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            type: "Identifier",
            name: "render",
          },
        });

        const param = findDeep(node?.value?.params, {
          key: {
            type: "Identifier",
            name: "type",
          },
        });

        expect(
          param,
          new CustomError({
            issue: `Не намираме параметъра type на render метода`,
            tips: [
              `Увери се, че render метода приема type аргумента`,
              `Увери се, че аргументите са в обект и се използва destructuring - ({ argName })`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should have а price argument in the render method in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            type: "Identifier",
            name: "render",
          },
        });

        const param = findDeep(node?.value?.params, {
          key: {
            type: "Identifier",
            name: "price",
          },
        });

        expect(
          param,
          new CustomError({
            issue: `Не намираме параметъра price на render метода`,
            tips: [
              `Увери се, че render метода приема price аргумента`,
              `Увери се, че аргументите са в обект и се използва destructuring - ({ argName })`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should have used classNames in the render method in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            type: "Identifier",
            name: "render",
          },
        }).deeper({
          type: "CallExpression",
          callee: {
            type: "Identifier",
            name: "classNames",
          },
        });

        expect(
          node,
          new CustomError({
            issue: `Излгежда, че не е използвам classNames в render метода`,
            tips: [
              `Увери се, че си използвал classNames за set-ването на класовете в render метода`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`Should have used formatCurrency in the render method in Notification`, () => {
    cy.task("readJSFileSync", { file: "src/js/Notification.js" }).then(
      (data) => {
        const node = findDeep(data, {
          key: {
            type: "Identifier",
            name: "render",
          },
        }).deeper({
          type: "CallExpression",
          callee: {
            type: "Identifier",
            name: "formatCurrency",
          },
        });

        expect(
          node,
          new CustomError({
            issue: `Излгежда, че не е използвам formatCurrency в render метода`,
            tips: [
              `Увери се, че си използвал formatCurrency за set-ването на класовете в render метода`,
            ],
          })
        ).to.exist;
      }
    );
  });
});
