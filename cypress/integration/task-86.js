const esprima = require("esprima");
const CustomError = require("../support/errors/CustomError");

context("task-86", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have ramda uninstalled from package.json", () => {
    cy.task("readFileSync", { file: "package.json" }).then((data) => {
      const pkg = JSON.parse(data);
      expect(
        pkg?.dependencies?.ramda,
        new CustomError({
          issue: "Ramda пакетът не е деинсталиран",
          tips: [
            "Увери се, че си деинсталирал ramda package",
            "Може да използваш командата npm uninstall --save <package_name>",
          ],
        })
      ).to.not.exist;
    });
  });

  it("should not import ramda", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      const tokens = esprima.parseModule(data);
      const imports = tokens.body.filter(
        ({ type }) => type === "ImportDeclaration"
      );
      expect(
        imports,
        new CustomError({
          issue: "Има повече от един import declaration",
          tips: ["Увери се, че единственият import declaration е за стиловете"],
        })
      ).to.have.length(1);
      expect(
        imports[0]?.source?.value,
        new CustomError({
          issue: "Стиловете са с неправилно наименование",
          tips: ["Увери се, че не си променил името на стиловете на Appa-a"],
        })
      ).to.equal("../scss/app.scss");
    });
  });

  it("should use the regular array filter method", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не се използва filter метода на масива",
          tips: [
            "Увери се, че използваш дефолтния филтър метод",
            "Увери се, си премахнал филтъра с ramda",
          ],
        })
      ).to.include(".filter(");
    });
  });

  it("should use document.createElement for displaying the array values in the unordered list", () => {
    cy.task("readFileSync", { file: "src/js/app.js" }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не се използва document.createElement",
          tips: [
            "Увери се, че използваш document.createElement за създаването на елементи",
            "Увери се, че добавяш създаваш 'li'",
          ],
        })
      ).to.include("document.createElement(");
    });
  });
});
