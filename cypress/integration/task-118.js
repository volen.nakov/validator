import CustomError from "../support/errors/CustomError";
import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";
import { findDeep } from "../support/utils";
import { testProps, testSCSS } from "../support/utils/react/tests";

const component = {
  name: "CollectorColumn",
  path: "src/components/collectors/CollectorColumn.jsx",
  props: {
    columnInfo: [
      {
        name: "Peter",
        info: 12312,
        avatar: "/images/avatar.png",
        verified: true,
        id: 1,
      },
      {
        name: "John",
        info: 1111,
        avatar: "/images/avatar.png",
        verified: true,
        id: 2,
      },
      {
        name: "Steven",
        info: 432,
        avatar: "/images/avatar.png",
        verified: true,
        id: 3,
      },
    ],
  },
};

const css = {
  path: "src/components/collectors/CollectorColumn.module.scss",
};

context("task-118", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["columnInfo"],
  });

  testSCSS({
    ...css,
  });

  it(`should have used the Collector component`, () => {
    mount({ ...component });
    cy.react("Collector");
  });

  it(`should have passed type prop to the Collector component`, () => {
    cy.task("readJSFileSync", { file: component.path, jsx: true }).then(
      (data) => {
        const result = findDeep(data, {
          name: { type: "JSXIdentifier", name: "type" },
        });

        expect(
          result,
          new CustomError({
            issue: `Не е подаден "type" prop на Collector компонента`,
            tips: [
              `Увери се, че освен props, които идват от горния компонент, добавяш и type prop`,
              `"type" prop ще ти е нужен да сменяш opacity-то на всеки втори Collector component в CollectorColumn`,
            ],
          })
        ).to.exist;
      }
    );
  });

  it(`should have passed a "light" class as a "type" prop to the Collector component`, () => {
    cy.task("readJSFileSync", { file: component.path, jsx: true }).then(
      (data) => {
        const result = findDeep(data, {
          name: { type: "JSXIdentifier", name: "type" },
        }).deeper({
          type: "Literal",
          value: "light",
          raw: '"light"',
        });

        expect(
          result,
          new CustomError({
            issue: `Не е подаден "light" клас на "type" prop`,
            tips: [
              `Увери се, че в зависимост от това дали текущият Collector component е втори по ред подаваш правилно "light"`,
              `Ако текущият Collector component не е вторият, може да подадеш празен стринг, но ако е трябва да подадеш "light"`,
            ],
          })
        ).to.exist;
      }
    );
  });
});
