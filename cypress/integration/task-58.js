const CustomError = require("../support/errors/CustomError");

context("task-58", () => {
  it("Should have a fixed navbar", () => {
    cy.visit(Cypress.config("url"));

    cy.get(".navbar").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Навигацията не е фиксирана",
          tips: [
            "Увери се, че съществува div с class 'navbar'",
            "Увери се, че е set-нат position: fixed на .navbar елемента",
            "Увери се, че си добавил стиловете в app.scss",
          ],
        })
      ).to.have.css("position", "fixed");
    });
  });
});
