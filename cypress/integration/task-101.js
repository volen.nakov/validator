const CustomError = require("../support/errors/CustomError");

context("task-101", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
    cy.waitForReact();
  });

  it("should have rendered App component", () => {
    cy.react("App").then((component) => {
      expect(
        component,
        new CustomError(CustomError.common.REACT_COMPONENT_NOT_RENDERED, null, {
          name: "App",
        })
      ).to.have.length(1);
    });
  });

  it("should have 2 onChange event handlers", () => {
    cy.task("readFileSync", {
      file: "src/App.js",
    }).then((data) => {
      const nodes = data.split("onChange={(");
      expect(
        nodes,
        new CustomError({
          issue: "Не е използван onChange event handler 2 пъти",
          tips: [
            "Увери се, че използваш onChange event handler-a 2 пъти тъй като имаш 2 инпута",
          ],
        })
      ).to.have.length(3);
    });
  });

  it("should have a onSubmit form handler", () => {
    cy.task("readFileSync", {
      file: "src/App.js",
    }).then((data) => {
      expect(
        data,
        new CustomError({
          issue: "Не е използван onSubmit handler",
          tips: [
            "Увери се, че използваш onSubmit handler във формата, тъй като е необходимо да имаш form handler function",
          ],
        })
      ).to.include("onSubmit={");
    });
  });

  it("should setIsVisible to true when the form is submitted", () => {
    cy.task("readFileSync", {
      file: "src/App.js",
    }).then((data) => {
      expect(
        data,
        new CustomError({
          issue:
            "Не се сетва setIsVisible на true, когато формата е събмитната",
          tips: [
            "Увери се, че след като формата е събмитната и е тригърната form handler function, трябва да се сетне setIsVisible на true",
          ],
        })
      )
        .to.include("setIsVisible(")
        .and.to.include("true");
    });
  });
});
