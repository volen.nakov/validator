const CustomError = require("../support/errors/CustomError");
import { findDeep } from "../support/utils";

context("task-63", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("Should have a defined sass map named widget-sizes in _vars.scss", () => {
    cy.task("readFileSync", { file: "src/scss/base/_vars.scss" }).then(
      (data) => {
        expect(
          data,
          new CustomError({
            issue: "Липсва $widget-sizes в _vars.scss",
            tips: [
              "Увери се, че си дефинирал sass map-a в правилния файл",
              "Увери се, че има разстояние между $widget-sizes: и ( в дефиницията",
              "Увери се, че си изписал името на map-a правилно",
            ],
          })
        ).to.include(`$widget-sizes:`);
      }
    );
  });

  it("Should get the value 'small' from the sass map $widget-sizes", () => {
    cy.task("readCSSFileSync", {
      file: "src/scss/app.scss",
      syntax: "scss",
    }).then((data) => {
      const node1 = findDeep(data, {
        type: "string",
        content: "'small'",
      });

      const node2 = findDeep(data, {
        type: "string",
        content: '"small"',
      });

      expect(
        node1 || node2,
        new CustomError({
          issue: "Не е използван map-get в app.scss",
          tips: [
            "Увери се, че използваш map-get",
            "Увери се, че си изписал името на sass map-a правилно",
            "Увери се, че си изписал името на property-то правилно",
          ],
        })
      ).to.exist;
    });
  });

  it("Should get the value 'large' from the sass map $widget-sizes", () => {
    cy.task("readCSSFileSync", {
      file: "src/scss/app.scss",
      syntax: "scss",
    }).then((data) => {
      const node1 = findDeep(data, {
        type: "string",
        content: "'large'",
      });

      const node2 = findDeep(data, {
        type: "string",
        content: '"large"',
      });

      expect(
        node1 || node2,
        new CustomError({
          issue: "Не е използван map-get в app.scss",
          tips: [
            "Увери се, че използваш map-get",
            "Увери се, че си изписал името на sass map-a правилно",
            "Увери се, че си изписал името на property-то правилно",
          ],
        })
      ).to.exist;
    });
  });
});
