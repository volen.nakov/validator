const CustomError = require("../support/errors/CustomError");

context("task-50", () => {
  it("Should have an input with a name 'name' and a type 'text'", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="name"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има type атрибут, който е 'text'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("type", "text");
    });
  });

  it("Should have an input with a name 'name' and a minlength of 20", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="name"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има minlength атрибут, който е със стойност 20",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("minlength", "20");
    });
  });

  it("Should have an input with a name 'name' and a required attribute", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="name"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има required атрибута",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("required");
    });
  });

  it("Should have an input with a name 'name_slug' and a required attribute", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="name_slug"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name_slug",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има required атрибута",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("required");
    });
  });

  it("Should have an input with a name 'name_slug' a pattern which matches kebab-case", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="name_slug"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут name_slug",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има pattern атрибут, който е със стойност '^([a-z][a-z0-9]*)(-[a-z0-9]+)*$'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("pattern", "^([a-z][a-z0-9]*)(-[a-z0-9]+)*$");
    });
  });

  it("Should have an input with a name 'contributers' and a required attribute", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="contributers"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут contributers",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има required атрибута",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("required");
    });
  });

  it("Should have an input with a name 'contributers' and 'min' attribute with a value of 1", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="contributers"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут contributers",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има min атрибут със стойност '1'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("min", "1");
    });
  });

  it("Should have an input with a name 'contributers' and 'max' attribute with a value of 100", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="contributers"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут contributers",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има max атрибут със стойност '100'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("max", "100");
    });
  });

  it("Should have an input with a name 'max_branches' and a required attribute", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="max_branches"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут max_branches",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има required атрибута",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("required");
    });
  });

  it("Should have an input with a name 'max_branches' and 'min' attribute with a value of 1", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="max_branches"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут max_branches",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има min атрибут със стойност '1'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("min", "1");
    });
  });

  it("Should have an input with a name 'max_branches' and 'max' attribute with a value of 100", () => {
    cy.visit(Cypress.config("url"));

    cy.get('.input[name="max_branches"]').then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Липсва input с name атрибут max_branches",
          tips: [
            "Увери се, че input-a има class 'input'",
            "Увери се, че input-a има max атрибут със стойност '100'",
            "Увери се, че input-a има name атрибут, което е 'name'",
          ],
        })
      ).to.have.attr("max", "100");
    });
  });
});
