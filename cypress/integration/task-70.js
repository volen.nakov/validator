const CustomError = require("../support/errors/CustomError");

context("task-70", () => {
  it(`should scale 2 times after being clicked`, () => {
    cy.visit(Cypress.config("url"));

    cy.get(".image").click();

    cy.get(".image").then((el) => {
      const element = el.get(0);
      const rect = element.getBoundingClientRect();
      // Check the difference between the element's transformed and original dimensions
      const scaleX = rect.width / element.offsetWidth;
      const scaleY = rect.height / element.offsetHeight;
      const error = new CustomError({
        issue: "Scale-a на .image не се е увеличил двойно след клик",
        tips: [
          "Увери се, че scale-ваш правилния елемент (.image)",
          "Увери се, че scale-ваш елемента с transform вместо да увеличаваш width и height",
        ],
      });
      expect(scaleX, error).to.eq(2);
      expect(scaleY, error).to.eq(2);
    });
  });
});
