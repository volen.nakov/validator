const CustomError = require("../support/errors/CustomError");

context("task-53", () => {
  it("should have an element with a product class", () => {
    cy.visit(Cypress.config("url"));
    cy.get(".product")
      .then(($el) => {
        expect(
          $el,
          new CustomError({
            issue: "Не съществува елемент с клас product",
            tips: ["Увери се, че не си пропуснал да добавиш клас product"],
          })
        ).to.exist;
      });
  });
  it("should have a data-price attribute", () => {
    cy.get(".product").then(($el) => {
      expect(
        $el,
        new CustomError({
          issue: "Не е сетнат data-price атрибут",
          tips: ["Увери се, че добавяш атрибута data-price на елемента с клас product"],
        })
      ).to.have.attr("data-price");
    });
  });
  it("should have an equal data-price attribute value to the span text", () => {
    cy.get(".price")
      .invoke("text")
      .then((text) => {
        cy.get(".product")
          .invoke("attr", "data-price")
          .then((dataPrice) => {
            expect(
              text,
              new CustomError({
                issue:
                  "Value-то на атрибута data-price не е идентично с value-то от span-a с клас price",
                tips: [
                  "Увери се, че правилно взимаш стойността на span-a с клас price",
                  "Провери как сетваш атрибута на елемента с клас product",
                ],
              })
            ).to.eq(dataPrice);
          });
      });
  });
});
