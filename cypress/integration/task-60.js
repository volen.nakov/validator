const CustomError = require("../support/errors/CustomError");

context("task-60", () => {
  it("Should have a navigation which is visible on viewports above 1024", () => {
    cy.visit(Cypress.config("url"));

    cy.viewport(1200, 720);

    cy.get(".navbar-menu").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Навигацията не е видима",
          tips: [
            "Увери се, че съществува div с class 'navbar-menu'",
            "Увери се, .navbar-menu е видимо над 1024px ширина на прозореца",
          ],
        })
      ).to.be.visible;
    });
  });

  it("Should have a navigation which is NOT visible on viewports below 600", () => {
    cy.visit(Cypress.config("url"));

    cy.viewport(599, 720);

    cy.get(".navbar-menu").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Навигацията е видима",
          tips: [
            "Увери се, че съществува div с class 'navbar-menu'",
            "Увери се, .navbar-menu НЕ е видимо под 1024px ширина на прозореца",
          ],
        })
      ).to.be.hidden;
    });
  });

  it("Should have a menu button visible on viewports below 600", () => {
    cy.visit(Cypress.config("url"));

    cy.viewport(599, 720);

    cy.get(".navbar-burger").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Menu бутона не е видим",
          tips: [
            "Увери се, че съществува div с class 'navbar-burger'",
            "Увери се, .navbar-burger е видим под 600px ширина на прозореца",
          ],
        })
      ).to.be.visible;
    });
  });
});
