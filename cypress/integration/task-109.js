import { testProps, testPropsDefaults } from "../support/utils/react/tests";

import CustomError from "../support/errors/CustomError";
import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "User",
  path: "src/components/user/User.jsx",
  props: {
    name: "Okay",
    info: "boomer",
    avatar: "/images/avatar.png",
    verified: true,
    size: 55,
  },
};

const css = {
  path: "src/components/user/User.module.scss",
};

context("task-109", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["size", "name", "info", "avatar", "verified"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "size",
        default: 55,
      },
      {
        name: "verified",
        default: false,
      },
    ],
  });

  it(`should have an element with a "user" css class`, () => {
    mount({ ...component });

    cy.react(component.name).then((comp) => {
      cy.wrap(comp).get(`div[class*="User_user_"]`);
    });
  });

  it(`should have used the Avatar component`, () => {
    mount({ ...component });
    cy.react("Avatar");
  });

  it(`should have rendered the user's name`, () => {
    mount({ ...component });

    cy.react(component.name).then((comp) => {
      cy.wrap(comp)
        .get(`[class*="User_name_"]`)
        .then((element) => {
          expect(
            element.get(0).innerText,
            new CustomError({
              issue: `Не се е рендерирало името на потребителя`,
              tips: [
                `Увери се, че съществува element с css class "name"`,
                `Увери се, че се визуализира стойноста на prop-a "name"`,
              ],
            })
          ).to.eq(component.props.name);
        });
    });
  });

  it(`should have rendered the user's info`, () => {
    mount({ ...component });

    cy.react(component.name).then((comp) => {
      cy.wrap(comp)
        .get(`[class*="User_info_"]`)
        .then((element) => {
          expect(
            element.get(0).innerText,
            new CustomError({
              issue: `Не се е рендерирало името на потребителя`,
              tips: [
                `Увери се, че съществува element с css class "info"`,
                `Увери се, че се визуализира стойноста на prop-a "info"`,
              ],
            })
          ).to.eq(component.props.info);
        });
    });
  });

  it(`should have а ${css.path} css module`, () => {
    cy.task("readFileSync", {
      file: css.path,
    });
  });

  it(`should have а used the colors variables`, () => {
    cy.task("readFileSync", {
      file: css.path,
    }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: `Не се използват variable-ите за цветовете в от styles/colors`,
          tips: [
            `Увери се, че import-ваш файла използвайки "@use"`,
            `Увери се, че import-ваш правилния файл`,
          ],
        })
      ).to.include(`@use "../../styles/colors";`);
    });
  });

  it(`should have а used "rem" for text sizing units`, () => {
    cy.task("readFileSync", {
      file: css.path,
    }).then((content) => {
      expect(
        content,
        new CustomError({
          issue: `Не се използват rem като мерни единици за текста`,
          tips: [
            `Увери се, че използваш точно rem вместо em, px, vw etc..`,
            `Увери се, че оразмеряваш текста както е зададено в дизайна на компонента`,
          ],
        })
      ).to.include(`rem;`);
    });
  });
});
