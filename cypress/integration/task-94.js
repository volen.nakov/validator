const CustomError = require("../support/errors/CustomError");
import { findDeep } from "../support/utils";

context("task-94", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have JS app", () => {
    cy.getJsApp(new CustomError(CustomError.common.JS_APP_NOT_FOUNT)).then(
      (app) => expect(app).to.exist
    );
  });

  it("should use map array method", () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          property: {
            type: "Identifier",
            name: "map",
          },
        });

        const mapExists = node?.property?.name;

        expect(
          mapExists,
          new CustomError({
            issue: "Не е използван map метода на масива",
            tips: [
              "Увери се, че използваш map, за да модифицираш първоначалния масив",
            ],
          })
        ).to.equal("map");
      }
    );
  });

  it("should create a paragraph element", () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          property: {
            type: "Identifier",
            name: "createElement",
          },
        });

        expect(
          node,
          new CustomError({
            issue: "Не е използван createElement за създаването на нов елемент",
            tips: [
              "Увери се, че си използвал document.createElement за създаването на нов елемент",
            ],
          })
        ).to.exist;
      }
    );
  });

  it("should append the created element to the wrapper div", () => {
    cy.task("readJSFileSync", { file: "src/js/Application.js" }).then(
      (data) => {
        const node = findDeep(data, {
          property: {
            type: "Identifier",
            name: "appendChild",
          },
        });

        expect(
          node,
          new CustomError({
            issue: "Не е използван appendChild",
            tips: [
              "Увери се, че използваш прикачаш елемента на wrapper div-a с appenChild",
            ],
          })
        );
      }
    );
  });
});
