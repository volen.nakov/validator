import {
  testProps,
  testPropsDefaults,
  testSCSS,
} from "../support/utils/react/tests";

import CustomError from "../support/errors/CustomError";
import { mount } from "../support/utils/nextjs/utils";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "Trending",
  path: "src/components/trending/Trending.jsx",
  props: {
    cards: [
      {
        name: "Clock",
        user: { avatarUrl: "images/avatar.png", verified: true },
        mediaUrl: "images/nft.jpg",
        price: 200,
        currency: "BTC",
      },
      {
        name: "DOGE",
        user: { avatarUrl: "images/avatar.png", verified: true },
        mediaUrl: "images/nft.jpg",
        price: 200,
        currency: "BTC",
      },
      {
        name: "BTC",
        user: { avatarUrl: "images/avatar.png", verified: true },
        mediaUrl: "images/nft.jpg",
        price: 100,
        currency: "BTC",
      },
      {
        name: "Litecoin",
        user: { avatarUrl: "images/avatar.png", verified: true },
        mediaUrl: "images/nft.jpg",
        price: 300,
        currency: "BTC",
      },
    ],
  },
};

const css = {
  path: "src/components/trending/Trending.module.scss",
};

context("task-111", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["cards"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "cards",
        default: [],
      },
    ],
  });

  testSCSS({
    ...css,
  });

  it(`should have used the Container component`, () => {
    mount({ ...component });
    cy.react("MuiContainerRoot");
  });

  it(`should have used the Select component`, () => {
    mount({ ...component });
    cy.react("MuiSelectSelect");
  });

  component.props.cards.forEach((card, index) => {
    it(`should have rendered card number ${index} with a name of ${card.name}`, () => {
      mount({ ...component });
      cy.get("[class*=Card_title]").then((el) => {
        expect(
          el[index]?.textContent,
          new CustomError({
            issue: `Не се е изрендирала правилната картинка на компонента`,
            tips: [
              `Увери се, че циклираш елементите от items масива и създаваш ImageListItem компоненти`,
              `Увери се, че подаваш правилния път към картинката`,
            ],
          })
        ).to.eq(card.name);
      });
    });
  });
});
