const CustomError = require("../support/errors/CustomError");

context("task-52", () => {
  it("Should have 5 elements with a class message after the body is clicked", () => {
    cy.visit(Cypress.config("url"));

    cy.get(".message").should("to.not.exist");

    cy.get("body").click();

    cy.get(".message").then((el) => {
      expect(
        el,
        new CustomError({
          issue: "Не съществуват 5 елемента с class 'message'",
          tips: [
            "Увери се, че има точно 5 елемента с class 'message' на страницата",
          ],
        })
      ).to.have.length(5);
    });
  });
});
