const CustomError = require("../support/errors/CustomError");
import { testProps, testPropsDefaults } from "../support/utils/react/tests";
import { testMount } from "../support/utils/nextjs/tests";

const component = {
  name: "ActivityItem",
  path: "src/components/activity/ActivityItem.jsx",
  props: {
    timeAgo: 5,
    user: {
      avatarUrl: "/images/avatar.png",
      verified: false,
      name: "Antonio Banderas",
    },
    likedNft: {
      name: "BTC",
      user: {
        name: "John Travolta",
        avatarUrl: "/images/avatar.png",
        verified: true,
      },
    },
    activityType: "buy",
  },
};

const css = {
  path: "src/components/activity/ActivityItem.module.scss",
};

context("task-117", () => {
  before(() => {
    cy.task("gitCheckout", { ref: "origin/dev", force: true });
  });

  testMount({
    ...component,
  });

  testProps({
    ...component,
    check: ["timeAgo", "user", "likedNft", "activityType"],
  });

  testPropsDefaults({
    ...component,
    check: [
      {
        name: "timeAgo",
        default: 0,
      },
    ],
  });

  it("should use the Avatar component", () => {
    cy.react("Avatar").then((comp) => {
      cy.wrap(comp)
        .find("img")
        .then((el) => {
          expect(
            el?.length,
            new CustomError({
              issue: "Значката verified се показва, когато не трябва",
              tips: [
                "Увери се, че user обекта, подаден като prop на Card компонента отговаря на props, които се подават на Avatar компонента",
              ],
            })
          ).to.equal(1);
        });
    });
  });
});
