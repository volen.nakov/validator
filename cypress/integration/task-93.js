const CustomError = require("../support/errors/CustomError");
const { findDeep } = require("../support/utils");

context("task-93", () => {
  before(() => {
    cy.visit(Cypress.config("url"));
  });

  it("should have JS app", () => {
    cy.getJsApp(new CustomError(CustomError.common.JS_APP_NOT_FOUNT)).then(
      (app) => expect(app).to.exist
    );
  });

  it("should use rest operator", () => {
    cy.task("readJSFileSync", {
      file: "src/js/Application.js",
      next: true,
    }).then((data) => {
      console.log(data);
    });
  });

  expect(true).to.be.true;

  //   it.only("should work with findDeep", () => {});
});
