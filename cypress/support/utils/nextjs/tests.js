import { mount } from "./utils";

/**
 * @param {String} name
 * @param {String} path
 */
export function testMount({ name, path, props }) {
  it(`should have an ${name} component which can be mounted`, () => {
    mount({
      name,
      path,
      props,
    });
  });
}
