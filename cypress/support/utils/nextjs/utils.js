/**
 * @param {String} name
 * @param {String} path
 * @param {Boolean} visit
 * @param {Boolean} wait
 */
export function mount({ name, path, props, visit = true, wait = true }) {
  cy.task(`nextMount`, {
    name,
    path,
    props,
  });
  visit && cy.visit(Cypress.config(`url`));
  wait && cy.waitForReact(1000, `#__next`);
}
