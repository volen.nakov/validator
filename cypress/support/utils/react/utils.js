const { findDeep } = require("../../utils");

/**
 * @param {Object} data - The parsed esprima data
 */
export function findPropsAll(data) {
  const props = findDeep(data, {
    type: "FunctionDeclaration",
  })?.params;

  return props?.[0]?.properties.map((prop) => {
    const left = prop.value?.left;
    const right = prop.value?.right;
    let value;

    // Handle different types of values set as default arguments
    switch (right?.type) {
      case "ArrayExpression":
        value = right.elements.map((i) => i.value);
        break;
    }

    let defaultProp = prop.value?.right?.value;

    if (!defaultProp && defaultProp !== 0 && defaultProp !== false) {
      defaultProp = prop.value?.right?.elements;
    }

    return {
      name: prop.value?.left?.name || prop.value?.name,
      default: defaultProp,
    };
  });
}

/**
 * @param {Object} data - The parsed esprima data
 * @param {Object} name - The name of the prop
 */
export function findProp(data, name) {
  const props = findPropsAll(data);
  return props.find((prop) => prop.name === name);
}
