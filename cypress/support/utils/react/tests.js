const CustomError = require("../../errors/CustomError");

import { findProp } from "./utils";

/**
 * @param {String} name
 * @param {String} path
 * @param {Array<String>} check
 */
export function testProps({ name, path, check }) {
  check.forEach((prop) => {
    it(`should have an ${name} component that accepts a '${prop}' prop`, () => {
      cy.task("readJSFileSync", {
        file: path,
        jsx: true,
      }).then((data) => {
        const result = findProp(data, prop);

        expect(
          result,
          new CustomError({
            issue: `Няма дефиниран ${prop} prop на ${name} компонента`,
            tips: [
              `Увери се, че ${name} е function component`,
              `Увери се, че компонентът приема prop с име ${prop}`,
              `Увери се, че компонентът приема prop във формата { ${prop} }`,
            ],
          })
        ).to.exist;
      });
    });
  });
}

/**
 * @param {String} name
 * @param {String} path
 * @param {Array<{ name: String, default: any }}>} props
 */
export function testPropsDefaults({ name, path, check }) {
  check.forEach((prop) => {
    it(`should have a default value for the '${prop.name}' prop`, () => {
      cy.task("readJSFileSync", {
        file: path,
        jsx: true,
      }).then((data) => {
        const result = findProp(data, prop.name);

        expect(
          result.default,
          new CustomError({
            issue: `Няма default стойност за ${prop.name} prop-а на ${name} компонента`,
            tips: [
              `Увери се, че задаваш default стойност ${prop.default}`,
              `Увери се, че default стойността се задава при деструктурирането на props`,
            ],
          })
        ).to.eql(prop.default);
      });
    });
  });
}

/**
 * @param {String} path
 */
export function testSCSS({ path }) {
  it(`should have а ${path} scss module`, () => {
    cy.task("readFileSync", {
      file: path,
    });
  });
}
