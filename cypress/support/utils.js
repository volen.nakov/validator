import CustomError from "./errors/CustomError";
import _ from "lodash";
import df from "d-forest";

export function timeOut(reject, event, interval = 100) {
  const timeLimit = Cypress.config().defaultCommandTimeout;
  let counter = 0;
  let intervalId = setInterval(() => {
    counter += interval;
    if (counter >= timeLimit) {
      const error = new CustomError({
        issue: `TimeOut при изчакване на event: ${event}`,
        tips: [`Увери се, че emit-ваш event с точното наименувание: ${event}`],
      });

      clearInterval(intervalId);
      reject(error);
    }
  }, interval);
}

/**
 * Performs a deep search in the data matching all key values provided in the search parameter
 * @param data
 * @param search
 * @returns {element}
 */
export function findDeep(data, search) {
  const result = df(data).findNode((node) => {
    return Object.keys(search).every((key) => {
      return _.isEqual(node[key], search[key]);
    });
  });

  if (result) {
    Object.assign(result, {
      deeper: (s) => findDeep(result, s),
    });
  }

  return result;
}
