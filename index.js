const exec = require("child_process").exec;
const clone = require("./src/clone");
const install = require("./src/install");
const symlink = require("./src/symlink");
const start = require("./src/start");

const REMOTE_URLS = ["https:", "http:", "git@"];
const noVideoTasks = [19, 20, 21, 22, 23, 24, 25];

const env = process.env;
const open = env.OPEN;
const taskId = env.TASK_ID;
const video = noVideoTasks.includes(parseInt(taskId))
  ? "--config-file no-video.cypress.json"
  : "";
const headed = env.HEADED ? "--headed" : "";
const url = env.REPO_URL;
const dir = `./.tmp`;

if (!env.RUN) process.exit(0);

(async () => {
  try {
    console.log(`Initiating tests for task ${taskId}...`);
    console.log(` DIRECTORY: ${dir}`);

    const isRemote = REMOTE_URLS.some((u) => url.startsWith(u));

    // If the url is local make a synlink instead to make development faster 💨
    if (isRemote) {
      await clone({ dir, url });
      await install({ dir });
    } else {
      await symlink({ dir, url });
    }

    // Run the start script if the project has one
    const package = require(`${dir}/package.json`);

    if (package.scripts.start) {
      // Some projects must not be started from the symlinked directory but
      // from the original folder
      await start({ dir: isRemote ? dir : url });
    }

    console.log(`Starting to run tests...`);
    // The Cypress task is a bit weird that's why we're using outside of primises
    const cmd = open
      ? `npm run cypress:open`
      : `npm run test -- ${headed} ${video} --spec "cypress/integration/task-${taskId}.js"`;
    const output = exec(cmd, { env: process.env });

    output.on("exit", (code) => {
      process.exit(code);
    });

    output.on("close", (code) => {
      process.exit(code);
    });

    output.stdout.on("data", (data) => {
      console.log(data);
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
