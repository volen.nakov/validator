# Validator

![boom.dev banner](https://boomcdn.fra1.digitaloceanspaces.com/c66381271ec3e55c06ec7c021ad81e1d.png)

This is the project which contains all the tests which validate the tasks for the tasks at boom.dev. Feel free to run it locally against your tasks and all contributions are welcome.

[<img src="https://img.shields.io/badge/cypress-6.0-green">](https://www.cypress.io/)
[<img src="https://img.shields.io/badge/💣 shaka-laka-blue">](https://booost.bg/)

# Setup

```bash
# Install
npm i

# Run tests
RUN=true HEADED=true TASK_ID={{TASK_ID}} REPO_URL={{REPO_URL}} node index.js

# Develop tests
RUN=true OPEN=true HEADED=true TASK_ID={{TASK_ID}} REPO_URL={{REPO_URL}} node index.js
```

### Variables

---

**{{RUN}}**

Whether to execute the test script or not. This is used for debugging the CI

---

**{{OPEN}}**

Whether to open the cypress window for test development and debugging

---

**{{HEADED}}**

Whether to execute the test script in headed mode (the cypress window being visible)

---

**{{TASK_ID}}**

This correlates with the name of the spec eg TASK_ID=1 will run spec cypress/integration/task-1.js

---

**{{REPO_URL}}**

The repository which will be cloned in /temp which tests will run against.

> **💡 TIP:** You can also provide a local address like `../local/project` so you avoid the clonning of the repository and installing its modules.

### Example

```bash
RUN=true OPEN=true TASK_ID=1 REPO_URL=https://gitlab.com/booost/boilerplates/app.git node index.js
```

# Configuration

Options located in `cypress.json`

| Property | Description                                                                                                                                    |
| -------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| url      | The url of the dev server which the project is running. We don't use baseUrl as some tasks (like the git eg. task-10) do not have dev servers. |
