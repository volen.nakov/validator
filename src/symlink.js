const rmfr = require("rmfr");
const symlinkDir = require("symlink-dir");
const CustomError = require("../cypress/support/errors/CustomError");

/**
 * Clones the project the tests will be cloned against
 * @param {String} url - The url of the git repository
 * @param {String} dir - The directory where the repository will be cloned
 */
module.exports = async ({ url, dir = "./" }) => {
  try {
    console.log(`Cleaning up the directory ${dir}...`);
    await rmfr(dir);

    console.log(`Creating symlink with local repository...`);
    await symlinkDir(url, dir);
  } catch (error) {
    throw new CustomError(
      {
        issue: `Не успяхме да направим symlink на repository-то repository-то`,
        tips: ["Нещо не е както трябва"],
      },
      error
    );
  }
};
